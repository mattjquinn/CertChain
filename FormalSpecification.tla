----------------------------- MODULE CertChain -----------------------------

\* This is the formal specification for the CertChain
\* peering and hashchain replication consensus protocol.
\*
\* When model checking, all declared constants should be
\* specified as model values, except for 'Nodes', which
\* should be a symmetrical set of model values corresponding
\* to the number of institutions with which you wish to model
\* check (i.e., {Stanford, Virginia, Tokyo}. Additionally, you
\* will likely want to specify a state constraint limiting the
\* the length of 'hashchains[n]' and 'channels[n]' for all
\* values of n; i.e.:
\*
\* \A n \in Nodes : Len(hashchains[n]) <= 100
\*               /\ Len(channels[n]) <= 3

EXTENDS Naturals, Sequences, TLC, FiniteSets

CONSTANT None
CONSTANTS SignatureRequest, SignatureResponse, PeerRequest, BlocksRequest, BlockManifest
CONSTANTS Nodes
CONSTANTS PeeringNotApproved, PeeringAwaitingOurApproval,
          PeeringAwaitingTheirApproval, PeeringApproved
CONSTANTS AddPeer, RemovePeer, Certify, Revoke

(***************************************************************************
--algorithm CertChain {

    variables channels = [i \in Nodes |-> << >>];
              hashchains = [i \in Nodes |-> << >>];
              replicas = [i \in Nodes |-> [j \in Nodes |-> << >>]];
              sigreqs_pending_sync = [i \in Nodes |-> << >>];
              manifests_pending_sync = [i \in Nodes |-> << >>];
              pending_actions = [i \in Nodes |-> << >>];
              processing_block = [i \in Nodes |-> None];
              peer_approvals = [i \in Nodes |-> [j \in Nodes |-> PeeringNotApproved]];
              blocks_request_queues = [i \in Nodes |-> << >>];
              
              \* These are used to simplify invariant checking,
              \* they are not needed in an implementation.
              hashchains_block_uid_set = [i \in Nodes |-> {}];
              replicas_block_uid_set = [i \in Nodes |-> [j \in Nodes |-> {}]];
    
    define {
        \* 2nd disjunct: replica may receive signed block before node move it to chain.
        \* 3rd disjunct: node may move block to chain without replica receiving the block
        \*               (but node won't be able to make another block without removing replica
        \*                as peer or sending missing block(s) over after sigreq).
        SafetyInvariant == \A n \in {a \in Nodes : hashchains[a] # << >>} 
            : \A s \in DOMAIN hashchains[n][Len(hashchains[n])].signatures 
                : replicas_block_uid_set[s][n] = hashchains_block_uid_set[n]
                    \/ (processing_block[n] # None /\ replicas_block_uid_set[s][n] \ hashchains_block_uid_set[n] = {processing_block[n].uid})
                    \/ hashchains_block_uid_set[n] \ replicas_block_uid_set[s][n] = {hashchains[n][Len(hashchains[n])].uid}
        
        Remove(i, seq) == [j \in 1..(Len(seq) - 1) |-> IF j < i THEN seq[j] ELSE seq[j + 1]]
    }
    
    macro SendNetMsg(m, chan) { chan := Append(chan, m) }
    
    
    \* This macro incorporates arbitrary duplication, deletion,
    \* and reordering of network messages, in addition to normal delivery.
    macro ReceiveNetMsg(v, chan) {
        await chan # <<>>;
        either {
            \* Drop the message.
            v := None;
            chan := Tail(chan);
        } or {
            \* Consume the message normally.
            v:= Head(chan);
            chan := Tail(chan);
        } or {
            \* Places duplicate at front of queue
            \* by not removing the message from the channel.
            v := Head(chan);
        } or {
            \* Places duplicate at back of queue.
            v := Head(chan);
            chan := Append(Tail(chan), Head(chan));
        } or {
            if (Len(chan) >= 2) {
              \* If two or messages are present, consumes the second
              \* and places the first at the back of the queue.
              v := chan[2];
              chan := Append(Tail(Tail(chan)), Head(chan));
            } else {
              \* Otherwise, returns no message.
              v := None;
            }
        }
    }
                         
    macro ComputeSignoffPeers(hashchain, actions) {
        signoff_peers := 
            ({s \in Nodes : \E b_idx_l \in 1..Len(hashchain)
                : \E a_l \in hashchain[b_idx_l].actions : a_l.atype = AddPeer /\ a_l.node = s
               /\ \A b_idx_r \in b_idx_l..Len(hashchain)
                    : ~(\E a_r \in hashchain[b_idx_r].actions : a_r.atype = RemovePeer /\ a_r.node = s)}
            \cup
            {s \in Nodes : \E m \in actions : m.atype = AddPeer /\ m.node = s})
            \
            {s \in Nodes : \E m \in actions : m.atype = RemovePeer /\ m.node = s}
    }
    
    macro ComputeCertifiedDocIds(hashchain, last_doc_id) {
        certified_doc_ids := 
            {id \in 1..last_doc_id : \E b_idx \in DOMAIN hashchain
                : \E a \in hashchain[b_idx].actions : a.atype = Certify /\ a.doc_id = id}
            \
            {id \in 1..last_doc_id : \E b_idx \in DOMAIN hashchain
                : \E a \in hashchain[b_idx].actions : a.atype = Revoke /\ a.doc_id = id}
    }
    
    macro ApprovePeeringRelationship(self, node_to_approve) {
        peer_approvals[self][node_to_approve] := PeeringApproved;
        actions := {[atype |-> AddPeer, node |-> node_to_approve]};
        pending_actions[self] := Append(pending_actions[self], actions);
    }
    
    macro IssueSignatureResponse(self, sigreq) {
        SendNetMsg([mtype |-> SignatureResponse,
              mfrom |-> self,
              signed_block_uid |-> sigreq.block.uid],
             channels[sigreq.mfrom]);
    }
    
    macro AppendBlockToChain(chain, block, block_uid_set) {
        assert (chain = << >>) <=> (block.parent = None);
        assert chain = << >> \/ chain[Len(chain)].uid = block.parent;
        chain := Append(chain, block);
        block_uid_set := block_uid_set \cup {block.uid};
    }
    
    \* @rcvdMsg : either a BlockManifest or a SignatureRequest
    macro RequestBlocksIfParentIsAbsent(rcvdMsg, replica, were_blocks_requested) {
        if ((replica = << >> /\ rcvdMsg.block.parent # None)
            \/ (replica # << >> /\ replica[Len(replica)].uid # rcvdMsg.block.parent)) {
                SendNetMsg([mtype |-> BlocksRequest,
                  mfrom |-> self,
                  after_block_uid |-> IF Len(replica) = 0 THEN None ELSE replica[Len(replica)].uid,
                  next_block_uid |-> IF Len(replica) = 0 THEN None ELSE replica[Len(replica)].uid],
                 channels[rcvdMsg.block.author]);
            were_blocks_requested := TRUE;
        } else {
            were_blocks_requested := FALSE;
        }
    }
                                               
    process (node \in Nodes)
    variables rcvdMsg = None; prev_block_uid = None; block_to_send = None;
              signoff_peers = {}; actions = None; next_doc_id = 1; with_set = {};
              were_blocks_requested = FALSE; certified_doc_ids = {}; peer_to_send = None;
    { daemon: while (TRUE) {
        either {
          \* Request peering with a node with which it has not been approved.
          \* Note: I am artificially restricting pending_actions
          \* and processing block to be empty so as to prevent state space blowup.
          await Len(pending_actions[self]) = 0 /\ processing_block[self] = None;
          with (n \in {a \in DOMAIN peer_approvals[self]
                  : a # self /\ peer_approvals[self][a] = PeeringNotApproved}) {
             SendNetMsg([mtype |-> PeerRequest,
                  mfrom |-> self], channels[n]);
             peer_approvals[self][n] := PeeringAwaitingTheirApproval;
          }
        } or {
          \* Cancel a peer request/end a peering relationship.
          \* Note: I am artificially restricting pending_actions
          \* and processing block to be empty so as to prevent state space blowup.
          await Len(pending_actions[self]) = 0 /\ processing_block[self] = None;
          with (n \in {a \in DOMAIN peer_approvals[self]
                : a # self /\ (peer_approvals[self][a] = PeeringApproved
                                \/ peer_approvals[self][a] = PeeringAwaitingOurApproval
                                \/ peer_approvals[self][a] = PeeringAwaitingTheirApproval)}) {
              \* If we/they are awaiting approval, this effectively denies their peer
              \* request or cancels our peer request, respectively.
              peer_approvals[self][n] := PeeringNotApproved;
              ComputeSignoffPeers(hashchains[self], {});
              if (n \in signoff_peers) {
                  \* If this node is one of our signoff peers, we queue a removal of it.
                  actions := {[atype |-> RemovePeer, node |-> n]};
                  pending_actions[self] := Append(pending_actions[self], actions);
              }
          }
        } or {
          \* Certify a document, as long as we have at least one peer.
          \* Note: I am artificially restricting pending_actions
          \* and processing block to be empty so as to prevent state space blowup.
          await Len(pending_actions[self]) = 0 /\ processing_block[self] = None;
          ComputeSignoffPeers(hashchains[self], {});
          await signoff_peers # {};
          actions := {[atype |-> Certify, doc_id |-> next_doc_id]};
          next_doc_id := next_doc_id + 1;
          pending_actions[self] := Append(pending_actions[self], actions);
        } or {
          \* Revoke a document, as long as we have at least
          \* one peer and one certified document.
          \* Note: I am artificially restricting pending_actions
          \* and processing block to be empty so as to prevent state space blowup.
          \* You must remember that if you remove these restrictions, you will
          \* have to check pending_actions and processing_block to ensure that
          \* a certified doc id hasn't already been revoked in those data structures.
          await Len(pending_actions[self]) = 0 /\ processing_block[self] = None;
          ComputeSignoffPeers(hashchains[self], {});
          await signoff_peers # {};
          ComputeCertifiedDocIds(hashchains[self], (next_doc_id-1));
          await certified_doc_ids # {};
          actions := {[atype |-> Revoke, doc_id |-> CHOOSE id \in certified_doc_ids : TRUE]};
          pending_actions[self] := Append(pending_actions[self], actions);
        } or {
          \* Approve peering with a node that has requested it of us.
          \* Note: I am artificially restricting pending_actions
          \* and processing block to be empty so as to prevent state space blowup.
          await Len(pending_actions[self]) = 0 /\ processing_block[self] = None;
          with (n \in {a \in DOMAIN peer_approvals[self] 
                : a # self /\ peer_approvals[self][a] = PeeringAwaitingOurApproval}) {
            ApprovePeeringRelationship(self, n);
          }
        } or {
            \* If one or more peers has yet to sign the processing block,
            \* we can abort it (prevents us from not being able to make any
            \* actions if one or more of our peers does not sign our blocks).
            await (processing_block[self] # None
                /\ \E s \in DOMAIN processing_block[self].signatures
                    : ~processing_block[self].signatures[s][2]);
            processing_block[self] := None;
        } or {
          \* If we have yet to request a signature from one of the signoff
          \* peers on the processing block, do so now. We make this a discrete
          \* step because we cannot send multiple SignatureRequests at the
          \* same time within the same step in which the processing block is created
          \* (recall that using a with stmt nondeterministcally chooses *only 1* possible
          \* value (peer), wherease here, we want to eventually send to all possible values (peers)).
          await (processing_block[self] # None);
          with (peer \in {s \in DOMAIN processing_block[self].signatures
                    : ~processing_block[self].signatures[s][1]}) {
            SendNetMsg([mtype |-> SignatureRequest,
                  mfrom |-> self,
                  block |-> processing_block[self]], channels[peer]);
            processing_block[self].signatures[peer][1] := TRUE;
          };
        } or {
          \* If the processing block has been signed by all of our peers,
          \* and we have yet to broadcast the block to one of them, do so now.
          \* This is a discrete step for the same reason that sigreq broadcasting
          \* is a discrete step; see comment for that step.
          await (processing_block[self] # None
                /\ \A s \in DOMAIN processing_block[self].signatures
                    : processing_block[self].signatures[s][1]
                   /\ processing_block[self].signatures[s][2]);
          with (peer \in {s \in DOMAIN processing_block[self].signatures
                    : ~processing_block[self].signatures[s][3]}) {
            SendNetMsg([mtype |-> BlockManifest,
                  block |-> processing_block[self]], channels[peer]);
            processing_block[self].signatures[peer][3] := TRUE;
          };
        } or {
          \* The processing block is added to the hashchain when all
          \* requested signatures have been received and it has been
          \* broadcast to all peers that signed off.
          await (processing_block[self] # None /\
                \A s \in DOMAIN processing_block[self].signatures
                    : processing_block[self].signatures[s][1]
                   /\ processing_block[self].signatures[s][2]
                   /\ processing_block[self].signatures[s][3]);
          AppendBlockToChain(hashchains[self], 
                processing_block[self], hashchains_block_uid_set[self]);
          processing_block[self] := None;
        } or {
          \* If we can start processing a new set of actions,
          \* we add the next pending list of actions to a new block and broadcast it
          \* to our peers.
          await (processing_block[self] = None /\ Len(pending_actions[self]) > 0);
          actions := Head(pending_actions[self]);
          pending_actions[self] := Tail(pending_actions[self]);
          
          ComputeSignoffPeers(hashchains[self], actions);
          assert self \notin signoff_peers; \* Cannot peer with ourself.
          
          processing_block[self] := 
                [parent |-> IF hashchains[self] = << >> THEN None
                    ELSE hashchains[self][Len(hashchains[self])].uid,
                 uid |-> IF prev_block_uid = None THEN 1 ELSE prev_block_uid + 1,
                 author |-> self,
                 actions |-> actions,
                 signatures |-> [i \in signoff_peers |-> <<FALSE, FALSE, FALSE>>]];
          prev_block_uid := IF prev_block_uid = None THEN 1 ELSE prev_block_uid + 1;
        } or {
          \* If a BlockRequest is queued, send the next block in the requested sequence.
          await Len(blocks_request_queues[self]) > 0;
          \* Note to self: it is OK to use deterministic CHOOSE here b/c nondeterminism
          \* is not needed/wanted.
          block_to_send := hashchains[self][CHOOSE b_idx \in 1..Len(hashchains[self]) 
                : hashchains[self][b_idx].parent = blocks_request_queues[self][1].next_block_uid];
          \* Send the block to the requesting node.
          SendNetMsg([mtype |-> BlockManifest,
                block |-> block_to_send],
               channels[blocks_request_queues[self][1].mfrom]);
          if (block_to_send = hashchains[self][Len(hashchains[self])]) {
            \* If this is the last block in our chain, deque the BlocksRequest.
            blocks_request_queues[self] := Tail(blocks_request_queues[self]);
          } else {
            \* If not the last block in our chain, update the BlocksRequest in preparation
            \* for the next execution of this step.
            blocks_request_queues[self][1].next_block_uid := block_to_send.uid;
          };
       } or {
          \* If a SignatureRequest that was put on hold during
          \* block sync can be responded to (specifically, if the current tip of
          \* the peer's replica is the parent of the sigreq's block),
          \* respond and remove it from the tuple.
          await Len(sigreqs_pending_sync[self]) > 0;
          with_set := {s_idx \in DOMAIN sigreqs_pending_sync[self] : 
               replicas[self][sigreqs_pending_sync[self][s_idx].mfrom] # << >>
            /\ replicas[self][sigreqs_pending_sync[self][s_idx].mfrom][
                  Len(replicas[self][sigreqs_pending_sync[self][s_idx].mfrom])].uid
                    = sigreqs_pending_sync[self][s_idx].block.parent};
          with (sigreq_idx \in with_set) {
            IssueSignatureResponse(self, sigreqs_pending_sync[self][sigreq_idx]);
            sigreqs_pending_sync[self] := Remove(sigreq_idx, sigreqs_pending_sync[self]);
          }
        } or {
            \* If a BlockManifest that was put on hold during
            \* block sync can be added to the chain (specifically, if the current tip of
            \* the peer's replica is the parent of the manifest's block), do so now.
            await Len(manifests_pending_sync[self]) > 0;
            with_set := {mf_idx \in DOMAIN manifests_pending_sync[self] : 
                 replicas[self][manifests_pending_sync[self][mf_idx].block.author] # << >>
              /\ replicas[self][manifests_pending_sync[self][mf_idx].block.author][
                    Len(replicas[self][manifests_pending_sync[self][mf_idx].block.author])].uid
                        = manifests_pending_sync[self][mf_idx].block.parent};
            with (mf_idx \in with_set) {
                AppendBlockToChain(
                    replicas[self][manifests_pending_sync[self][mf_idx].block.author],
                    manifests_pending_sync[self][mf_idx].block,
                    replicas_block_uid_set[self][manifests_pending_sync[self][mf_idx].block.author]);
                manifests_pending_sync[self] := Remove(mf_idx, manifests_pending_sync[self]);
            }
        } or {
          \* A message is received and processed from another node.
          ReceiveNetMsg(rcvdMsg, channels[self]);
          procmsg: if (rcvdMsg = None) {
            skip;
          } else if (rcvdMsg.mtype = SignatureRequest
              /\ (peer_approvals[self][rcvdMsg.mfrom] = PeeringApproved
                   \/ (peer_approvals[self][rcvdMsg.mfrom] = PeeringAwaitingTheirApproval
                        \* If we are awaiting their approval, we only process sigreqs
                        \* for blocks in which they add us as a peer.
                        /\ \E action \in rcvdMsg.block.actions
                                : action.atype = AddPeer /\ action.node = self))
              /\ ~(\E b_idx \in DOMAIN replicas[self][rcvdMsg.block.author]
                    : replicas[self][rcvdMsg.block.author][b_idx].parent = rcvdMsg.block.parent)
              /\ ~(\E sr_idx \in DOMAIN sigreqs_pending_sync[self] 
                    : sigreqs_pending_sync[self][sr_idx].block.uid = rcvdMsg.block.uid
                   /\ sigreqs_pending_sync[self][sr_idx].block.author = rcvdMsg.block.author)) {
            \* A request for our signature on a peer's block is handled.
            \* We ignore sigreqs for:
            \*   - blocks with the same parent as another block in the replica.
            \*   - blocks that are pending replica sync.
            if (peer_approvals[self][rcvdMsg.mfrom] = PeeringAwaitingTheirApproval) {
                \* Due to guard above, they have added us a peer in the attached block,
                \* so we approve the relationship on our end as well.
                ApprovePeeringRelationship(self, rcvdMsg.mfrom);
            };
            RequestBlocksIfParentIsAbsent(rcvdMsg, replicas[self][rcvdMsg.mfrom], were_blocks_requested);
            sync_or_sign: if (were_blocks_requested) {
                \* We queue the SignatureRequest for responding to at a later time.
                sigreqs_pending_sync[self] := Append(sigreqs_pending_sync[self], rcvdMsg);
            } else {
                IssueSignatureResponse(self, rcvdMsg);
            }
          } else if (rcvdMsg.mtype = SignatureResponse) {
            \* A signature for a block we requested is handled; we must first check to ensure
            \* that it is a signature for our current processing block.
            if (processing_block[self] # None
                /\ rcvdMsg.signed_block_uid = processing_block[self].uid) {
              processing_block[self].signatures[rcvdMsg.mfrom][2] := TRUE;
            }
          } else if (rcvdMsg.mtype = PeerRequest) {
            \* A request to peer with another institution is handled.
            if (peer_approvals[self][rcvdMsg.mfrom] = PeeringNotApproved) {
                peer_approvals[self][rcvdMsg.mfrom] := PeeringAwaitingOurApproval;
            } else if (peer_approvals[self][rcvdMsg.mfrom] = PeeringAwaitingTheirApproval) {
                \* It's possible, though not likely, that two nodes request each other
                \* simultaneously such that both are awaiting the other's approval. If this
                \* happens, upon receiving the other's PeerRequest, we automatically
                \* approve the peering relationship.
                ApprovePeeringRelationship(self, rcvdMsg.mfrom);
            } else if (peer_approvals[self][rcvdMsg.mfrom] = PeeringApproved) {
                \* Receiving a PeerRequest from a node we already consider approved
                \* could be due to a node re-sending their PeerRequest, or a node
                \* that didn't receive the SignatureRequest we sent them, or a node
                \* we stopped peering with but thinks we are still their peer.
                ComputeSignoffPeers(hashchains[self], {});
                if (rcvdMsg.mfrom \in signoff_peers) {
                    \* If we already consider the node to be one of our signoff peers,
                    \* we end our current peering relationship with them and start a new one.
                    pending_actions[self] := Append(
                        Append(pending_actions[self],
                            {[atype |-> RemovePeer, node |-> rcvdMsg.mfrom]}),
                        {[atype |-> AddPeer, node |-> rcvdMsg.mfrom]});
                } else if (processing_block[self] # None
                    /\ \E a \in processing_block[self].actions : a.atype = AddPeer
                                                              /\ a.node = rcvdMsg.mfrom) {
                    \* If we are adding the node as a peer in our processing block, (re)send
                    \* the processing block in a sigreq to them.
                    SendNetMsg([mtype |-> SignatureRequest,
                        mfrom |-> self,
                        block |-> processing_block[self]], channels[rcvdMsg.mfrom]);                                   
                } else {
                   \* If we're here, that means an admin approved the peering relationship
                   \* but we never actually added an AddPeer to our chain for the node
                   \* (we likely aborted the block when it was processing). Re-queue an
                   \* AddPeer action.
                   ApprovePeeringRelationship(self, rcvdMsg.mfrom);
                }
            } else if (peer_approvals[self][rcvdMsg.mfrom] = PeeringAwaitingOurApproval) {
                \* If we're waiting for admin approval, ignore this repeated PeerRequest.
                skip;
            }
          } else if (rcvdMsg.mtype = BlocksRequest) {
            \* A request for one or more blocks in our hashchain is handled;
            blocks_request_queues[self] := Append(blocks_request_queues[self], rcvdMsg);
          } else if (rcvdMsg.mtype = BlockManifest
              /\ ~(\E b_idx \in DOMAIN replicas[self][rcvdMsg.block.author] 
                    : replicas[self][rcvdMsg.block.author][b_idx].uid = rcvdMsg.block.uid)
              /\ ~(\E mf_idx \in DOMAIN manifests_pending_sync[self] 
                    : manifests_pending_sync[self][mf_idx].block.uid = rcvdMsg.block.uid
                   /\ manifests_pending_sync[self][mf_idx].block.author = rcvdMsg.block.author)) {
            \* A block is sent to us, either in response to a BlocksRequest issued by us
            \* or upon inclusion of a new block by one of our peers in their hashchain.
            \* We ignore blocks already in our chain / pending sync, per above conditional.
            \* Ensure that the block has been signed by all peers.
            assert \A s \in DOMAIN rcvdMsg.block.signatures : rcvdMsg.block.signatures[s][2];
            RequestBlocksIfParentIsAbsent(rcvdMsg, replicas[self][rcvdMsg.block.author], were_blocks_requested);
            if (were_blocks_requested) {
              manifests_pending_sync[self] := Append(manifests_pending_sync[self], rcvdMsg);
            } else {
              AppendBlockToChain(replicas[self][rcvdMsg.block.author],
                rcvdMsg.block, replicas_block_uid_set[self][rcvdMsg.block.author]);
            }
          }
       }
      }
    }
}

 ***************************************************************************)
\* BEGIN TRANSLATION
VARIABLES channels, hashchains, replicas, sigreqs_pending_sync, 
          manifests_pending_sync, pending_actions, processing_block, 
          peer_approvals, blocks_request_queues, hashchains_block_uid_set, 
          replicas_block_uid_set, pc

(* define statement *)
SafetyInvariant == \A n \in {a \in Nodes : hashchains[a] # << >>}
    : \A s \in DOMAIN hashchains[n][Len(hashchains[n])].signatures
        : replicas_block_uid_set[s][n] = hashchains_block_uid_set[n]
            \/ (processing_block[n] # None /\ replicas_block_uid_set[s][n] \ hashchains_block_uid_set[n] = {processing_block[n].uid})
            \/ hashchains_block_uid_set[n] \ replicas_block_uid_set[s][n] = {hashchains[n][Len(hashchains[n])].uid}

Remove(i, seq) == [j \in 1..(Len(seq) - 1) |-> IF j < i THEN seq[j] ELSE seq[j + 1]]

VARIABLES rcvdMsg, prev_block_uid, block_to_send, signoff_peers, actions, 
          next_doc_id, with_set, were_blocks_requested, certified_doc_ids, 
          peer_to_send

vars == << channels, hashchains, replicas, sigreqs_pending_sync, 
           manifests_pending_sync, pending_actions, processing_block, 
           peer_approvals, blocks_request_queues, hashchains_block_uid_set, 
           replicas_block_uid_set, pc, rcvdMsg, prev_block_uid, block_to_send, 
           signoff_peers, actions, next_doc_id, with_set, 
           were_blocks_requested, certified_doc_ids, peer_to_send >>

ProcSet == (Nodes)

Init == (* Global variables *)
        /\ channels = [i \in Nodes |-> << >>]
        /\ hashchains = [i \in Nodes |-> << >>]
        /\ replicas = [i \in Nodes |-> [j \in Nodes |-> << >>]]
        /\ sigreqs_pending_sync = [i \in Nodes |-> << >>]
        /\ manifests_pending_sync = [i \in Nodes |-> << >>]
        /\ pending_actions = [i \in Nodes |-> << >>]
        /\ processing_block = [i \in Nodes |-> None]
        /\ peer_approvals = [i \in Nodes |-> [j \in Nodes |-> PeeringNotApproved]]
        /\ blocks_request_queues = [i \in Nodes |-> << >>]
        /\ hashchains_block_uid_set = [i \in Nodes |-> {}]
        /\ replicas_block_uid_set = [i \in Nodes |-> [j \in Nodes |-> {}]]
        (* Process node *)
        /\ rcvdMsg = [self \in Nodes |-> None]
        /\ prev_block_uid = [self \in Nodes |-> None]
        /\ block_to_send = [self \in Nodes |-> None]
        /\ signoff_peers = [self \in Nodes |-> {}]
        /\ actions = [self \in Nodes |-> None]
        /\ next_doc_id = [self \in Nodes |-> 1]
        /\ with_set = [self \in Nodes |-> {}]
        /\ were_blocks_requested = [self \in Nodes |-> FALSE]
        /\ certified_doc_ids = [self \in Nodes |-> {}]
        /\ peer_to_send = [self \in Nodes |-> None]
        /\ pc = [self \in ProcSet |-> "daemon"]

daemon(self) == /\ pc[self] = "daemon"
                /\ \/ /\ Len(pending_actions[self]) = 0 /\ processing_block[self] = None
                      /\ \E n \in     {a \in DOMAIN peer_approvals[self]
                                  : a # self /\ peer_approvals[self][a] = PeeringNotApproved}:
                           /\ channels' = [channels EXCEPT ![n] = Append((channels[n]), (      [mtype |-> PeerRequest,
                                                                  mfrom |-> self]))]
                           /\ peer_approvals' = [peer_approvals EXCEPT ![self][n] = PeeringAwaitingTheirApproval]
                      /\ pc' = [pc EXCEPT ![self] = "daemon"]
                      /\ UNCHANGED <<hashchains, replicas, sigreqs_pending_sync, manifests_pending_sync, pending_actions, processing_block, blocks_request_queues, hashchains_block_uid_set, replicas_block_uid_set, rcvdMsg, prev_block_uid, block_to_send, signoff_peers, actions, next_doc_id, with_set, certified_doc_ids>>
                   \/ /\ Len(pending_actions[self]) = 0 /\ processing_block[self] = None
                      /\ \E n \in       {a \in DOMAIN peer_approvals[self]
                                  : a # self /\ (peer_approvals[self][a] = PeeringApproved
                                                  \/ peer_approvals[self][a] = PeeringAwaitingOurApproval
                                                  \/ peer_approvals[self][a] = PeeringAwaitingTheirApproval)}:
                           /\ peer_approvals' = [peer_approvals EXCEPT ![self][n] = PeeringNotApproved]
                           /\ signoff_peers' = [signoff_peers EXCEPT ![self] = ({s \in Nodes : \E b_idx_l \in 1..Len((hashchains[self]))
                                                                                   : \E a_l \in (hashchains[self])[b_idx_l].actions : a_l.atype = AddPeer /\ a_l.node = s
                                                                                  /\ \A b_idx_r \in b_idx_l..Len((hashchains[self]))
                                                                                       : ~(\E a_r \in (hashchains[self])[b_idx_r].actions : a_r.atype = RemovePeer /\ a_r.node = s)}
                                                                               \cup
                                                                               {s \in Nodes : \E m \in ({}) : m.atype = AddPeer /\ m.node = s})
                                                                               \
                                                                               {s \in Nodes : \E m \in ({}) : m.atype = RemovePeer /\ m.node = s}]
                           /\ IF n \in signoff_peers'[self]
                                 THEN /\ actions' = [actions EXCEPT ![self] = {[atype |-> RemovePeer, node |-> n]}]
                                      /\ pending_actions' = [pending_actions EXCEPT ![self] = Append(pending_actions[self], actions'[self])]
                                 ELSE /\ TRUE
                                      /\ UNCHANGED << pending_actions, actions >>
                      /\ pc' = [pc EXCEPT ![self] = "daemon"]
                      /\ UNCHANGED <<channels, hashchains, replicas, sigreqs_pending_sync, manifests_pending_sync, processing_block, blocks_request_queues, hashchains_block_uid_set, replicas_block_uid_set, rcvdMsg, prev_block_uid, block_to_send, next_doc_id, with_set, certified_doc_ids>>
                   \/ /\ Len(pending_actions[self]) = 0 /\ processing_block[self] = None
                      /\ signoff_peers' = [signoff_peers EXCEPT ![self] = ({s \in Nodes : \E b_idx_l \in 1..Len((hashchains[self]))
                                                                              : \E a_l \in (hashchains[self])[b_idx_l].actions : a_l.atype = AddPeer /\ a_l.node = s
                                                                             /\ \A b_idx_r \in b_idx_l..Len((hashchains[self]))
                                                                                  : ~(\E a_r \in (hashchains[self])[b_idx_r].actions : a_r.atype = RemovePeer /\ a_r.node = s)}
                                                                          \cup
                                                                          {s \in Nodes : \E m \in ({}) : m.atype = AddPeer /\ m.node = s})
                                                                          \
                                                                          {s \in Nodes : \E m \in ({}) : m.atype = RemovePeer /\ m.node = s}]
                      /\ signoff_peers'[self] # {}
                      /\ actions' = [actions EXCEPT ![self] = {[atype |-> Certify, doc_id |-> next_doc_id[self]]}]
                      /\ next_doc_id' = [next_doc_id EXCEPT ![self] = next_doc_id[self] + 1]
                      /\ pending_actions' = [pending_actions EXCEPT ![self] = Append(pending_actions[self], actions'[self])]
                      /\ pc' = [pc EXCEPT ![self] = "daemon"]
                      /\ UNCHANGED <<channels, hashchains, replicas, sigreqs_pending_sync, manifests_pending_sync, processing_block, peer_approvals, blocks_request_queues, hashchains_block_uid_set, replicas_block_uid_set, rcvdMsg, prev_block_uid, block_to_send, with_set, certified_doc_ids>>
                   \/ /\ Len(pending_actions[self]) = 0 /\ processing_block[self] = None
                      /\ signoff_peers' = [signoff_peers EXCEPT ![self] = ({s \in Nodes : \E b_idx_l \in 1..Len((hashchains[self]))
                                                                              : \E a_l \in (hashchains[self])[b_idx_l].actions : a_l.atype = AddPeer /\ a_l.node = s
                                                                             /\ \A b_idx_r \in b_idx_l..Len((hashchains[self]))
                                                                                  : ~(\E a_r \in (hashchains[self])[b_idx_r].actions : a_r.atype = RemovePeer /\ a_r.node = s)}
                                                                          \cup
                                                                          {s \in Nodes : \E m \in ({}) : m.atype = AddPeer /\ m.node = s})
                                                                          \
                                                                          {s \in Nodes : \E m \in ({}) : m.atype = RemovePeer /\ m.node = s}]
                      /\ signoff_peers'[self] # {}
                      /\ certified_doc_ids' = [certified_doc_ids EXCEPT ![self] = {id \in 1..((next_doc_id[self]-1)) : \E b_idx \in DOMAIN (hashchains[self])
                                                                                      : \E a \in (hashchains[self])[b_idx].actions : a.atype = Certify /\ a.doc_id = id}
                                                                                  \
                                                                                  {id \in 1..((next_doc_id[self]-1)) : \E b_idx \in DOMAIN (hashchains[self])
                                                                                      : \E a \in (hashchains[self])[b_idx].actions : a.atype = Revoke /\ a.doc_id = id}]
                      /\ certified_doc_ids'[self] # {}
                      /\ actions' = [actions EXCEPT ![self] = {[atype |-> Revoke, doc_id |-> CHOOSE id \in certified_doc_ids'[self] : TRUE]}]
                      /\ pending_actions' = [pending_actions EXCEPT ![self] = Append(pending_actions[self], actions'[self])]
                      /\ pc' = [pc EXCEPT ![self] = "daemon"]
                      /\ UNCHANGED <<channels, hashchains, replicas, sigreqs_pending_sync, manifests_pending_sync, processing_block, peer_approvals, blocks_request_queues, hashchains_block_uid_set, replicas_block_uid_set, rcvdMsg, prev_block_uid, block_to_send, next_doc_id, with_set>>
                   \/ /\ Len(pending_actions[self]) = 0 /\ processing_block[self] = None
                      /\ \E n \in       {a \in DOMAIN peer_approvals[self]
                                  : a # self /\ peer_approvals[self][a] = PeeringAwaitingOurApproval}:
                           /\ peer_approvals' = [peer_approvals EXCEPT ![self][n] = PeeringApproved]
                           /\ actions' = [actions EXCEPT ![self] = {[atype |-> AddPeer, node |-> n]}]
                           /\ pending_actions' = [pending_actions EXCEPT ![self] = Append(pending_actions[self], actions'[self])]
                      /\ pc' = [pc EXCEPT ![self] = "daemon"]
                      /\ UNCHANGED <<channels, hashchains, replicas, sigreqs_pending_sync, manifests_pending_sync, processing_block, blocks_request_queues, hashchains_block_uid_set, replicas_block_uid_set, rcvdMsg, prev_block_uid, block_to_send, signoff_peers, next_doc_id, with_set, certified_doc_ids>>
                   \/ /\   (processing_block[self] # None
                         /\ \E s \in DOMAIN processing_block[self].signatures
                             : ~processing_block[self].signatures[s][2])
                      /\ processing_block' = [processing_block EXCEPT ![self] = None]
                      /\ pc' = [pc EXCEPT ![self] = "daemon"]
                      /\ UNCHANGED <<channels, hashchains, replicas, sigreqs_pending_sync, manifests_pending_sync, pending_actions, peer_approvals, blocks_request_queues, hashchains_block_uid_set, replicas_block_uid_set, rcvdMsg, prev_block_uid, block_to_send, signoff_peers, actions, next_doc_id, with_set, certified_doc_ids>>
                   \/ /\ (processing_block[self] # None)
                      /\ \E peer \in      {s \in DOMAIN processing_block[self].signatures
                                     : ~processing_block[self].signatures[s][1]}:
                           /\ channels' = [channels EXCEPT ![peer] = Append((channels[peer]), (     [mtype |-> SignatureRequest,
                                                                     mfrom |-> self,
                                                                     block |-> processing_block[self]]))]
                           /\ processing_block' = [processing_block EXCEPT ![self].signatures[peer][1] = TRUE]
                      /\ pc' = [pc EXCEPT ![self] = "daemon"]
                      /\ UNCHANGED <<hashchains, replicas, sigreqs_pending_sync, manifests_pending_sync, pending_actions, peer_approvals, blocks_request_queues, hashchains_block_uid_set, replicas_block_uid_set, rcvdMsg, prev_block_uid, block_to_send, signoff_peers, actions, next_doc_id, with_set, certified_doc_ids>>
                   \/ /\ (processing_block[self] # None
                         /\ \A s \in DOMAIN processing_block[self].signatures
                             : processing_block[self].signatures[s][1]
                            /\ processing_block[self].signatures[s][2])
                      /\ \E peer \in      {s \in DOMAIN processing_block[self].signatures
                                     : ~processing_block[self].signatures[s][3]}:
                           /\ channels' = [channels EXCEPT ![peer] = Append((channels[peer]), (     [mtype |-> BlockManifest,
                                                                     block |-> processing_block[self]]))]
                           /\ processing_block' = [processing_block EXCEPT ![self].signatures[peer][3] = TRUE]
                      /\ pc' = [pc EXCEPT ![self] = "daemon"]
                      /\ UNCHANGED <<hashchains, replicas, sigreqs_pending_sync, manifests_pending_sync, pending_actions, peer_approvals, blocks_request_queues, hashchains_block_uid_set, replicas_block_uid_set, rcvdMsg, prev_block_uid, block_to_send, signoff_peers, actions, next_doc_id, with_set, certified_doc_ids>>
                   \/ /\ (processing_block[self] # None /\
                         \A s \in DOMAIN processing_block[self].signatures
                             : processing_block[self].signatures[s][1]
                            /\ processing_block[self].signatures[s][2]
                            /\ processing_block[self].signatures[s][3])
                      /\ Assert(((hashchains[self]) = << >>) <=> ((processing_block[self]).parent = None), 
                                "Failure of assertion at line 130, column 9 of macro called at line 267, column 11.")
                      /\ Assert((hashchains[self]) = << >> \/ (hashchains[self])[Len((hashchains[self]))].uid = (processing_block[self]).parent, 
                                "Failure of assertion at line 131, column 9 of macro called at line 267, column 11.")
                      /\ hashchains' = [hashchains EXCEPT ![self] = Append((hashchains[self]), (processing_block[self]))]
                      /\ hashchains_block_uid_set' = [hashchains_block_uid_set EXCEPT ![self] = (hashchains_block_uid_set[self]) \cup {(processing_block[self]).uid}]
                      /\ processing_block' = [processing_block EXCEPT ![self] = None]
                      /\ pc' = [pc EXCEPT ![self] = "daemon"]
                      /\ UNCHANGED <<channels, replicas, sigreqs_pending_sync, manifests_pending_sync, pending_actions, peer_approvals, blocks_request_queues, replicas_block_uid_set, rcvdMsg, prev_block_uid, block_to_send, signoff_peers, actions, next_doc_id, with_set, certified_doc_ids>>
                   \/ /\ (processing_block[self] = None /\ Len(pending_actions[self]) > 0)
                      /\ actions' = [actions EXCEPT ![self] = Head(pending_actions[self])]
                      /\ pending_actions' = [pending_actions EXCEPT ![self] = Tail(pending_actions[self])]
                      /\ signoff_peers' = [signoff_peers EXCEPT ![self] = ({s \in Nodes : \E b_idx_l \in 1..Len((hashchains[self]))
                                                                              : \E a_l \in (hashchains[self])[b_idx_l].actions : a_l.atype = AddPeer /\ a_l.node = s
                                                                             /\ \A b_idx_r \in b_idx_l..Len((hashchains[self]))
                                                                                  : ~(\E a_r \in (hashchains[self])[b_idx_r].actions : a_r.atype = RemovePeer /\ a_r.node = s)}
                                                                          \cup
                                                                          {s \in Nodes : \E m \in actions'[self] : m.atype = AddPeer /\ m.node = s})
                                                                          \
                                                                          {s \in Nodes : \E m \in actions'[self] : m.atype = RemovePeer /\ m.node = s}]
                      /\ Assert(self \notin signoff_peers'[self], 
                                "Failure of assertion at line 279, column 11.")
                      /\ processing_block' = [processing_block EXCEPT ![self] = [parent |-> IF hashchains[self] = << >> THEN None
                                                                                    ELSE hashchains[self][Len(hashchains[self])].uid,
                                                                                 uid |-> IF prev_block_uid[self] = None THEN 1 ELSE prev_block_uid[self] + 1,
                                                                                 author |-> self,
                                                                                 actions |-> actions'[self],
                                                                                 signatures |-> [i \in signoff_peers'[self] |-> <<FALSE, FALSE, FALSE>>]]]
                      /\ prev_block_uid' = [prev_block_uid EXCEPT ![self] = IF prev_block_uid[self] = None THEN 1 ELSE prev_block_uid[self] + 1]
                      /\ pc' = [pc EXCEPT ![self] = "daemon"]
                      /\ UNCHANGED <<channels, hashchains, replicas, sigreqs_pending_sync, manifests_pending_sync, peer_approvals, blocks_request_queues, hashchains_block_uid_set, replicas_block_uid_set, rcvdMsg, block_to_send, next_doc_id, with_set, certified_doc_ids>>
                   \/ /\ Len(blocks_request_queues[self]) > 0
                      /\ block_to_send' = [block_to_send EXCEPT ![self] =            hashchains[self][CHOOSE b_idx \in 1..Len(hashchains[self])
                                                                          : hashchains[self][b_idx].parent = blocks_request_queues[self][1].next_block_uid]]
                      /\ channels' = [channels EXCEPT ![blocks_request_queues[self][1].mfrom] = Append((channels[blocks_request_queues[self][1].mfrom]), (     [mtype |-> BlockManifest,
                                                                                                block |-> block_to_send'[self]]))]
                      /\ IF block_to_send'[self] = hashchains[self][Len(hashchains[self])]
                            THEN /\ blocks_request_queues' = [blocks_request_queues EXCEPT ![self] = Tail(blocks_request_queues[self])]
                            ELSE /\ blocks_request_queues' = [blocks_request_queues EXCEPT ![self][1].next_block_uid = block_to_send'[self].uid]
                      /\ pc' = [pc EXCEPT ![self] = "daemon"]
                      /\ UNCHANGED <<hashchains, replicas, sigreqs_pending_sync, manifests_pending_sync, pending_actions, processing_block, peer_approvals, hashchains_block_uid_set, replicas_block_uid_set, rcvdMsg, prev_block_uid, signoff_peers, actions, next_doc_id, with_set, certified_doc_ids>>
                   \/ /\ Len(sigreqs_pending_sync[self]) > 0
                      /\ with_set' = [with_set EXCEPT ![self] =           {s_idx \in DOMAIN sigreqs_pending_sync[self] :
                                                                   replicas[self][sigreqs_pending_sync[self][s_idx].mfrom] # << >>
                                                                /\ replicas[self][sigreqs_pending_sync[self][s_idx].mfrom][
                                                                      Len(replicas[self][sigreqs_pending_sync[self][s_idx].mfrom])].uid
                                                                        = sigreqs_pending_sync[self][s_idx].block.parent}]
                      /\ \E sigreq_idx \in with_set'[self]:
                           /\ channels' = [channels EXCEPT ![(sigreqs_pending_sync[self][sigreq_idx]).mfrom] = Append((channels[(sigreqs_pending_sync[self][sigreq_idx]).mfrom]), (     [mtype |-> SignatureResponse,
                                                                                                               mfrom |-> self,
                                                                                                               signed_block_uid |-> (sigreqs_pending_sync[self][sigreq_idx]).block.uid]))]
                           /\ sigreqs_pending_sync' = [sigreqs_pending_sync EXCEPT ![self] = Remove(sigreq_idx, sigreqs_pending_sync[self])]
                      /\ pc' = [pc EXCEPT ![self] = "daemon"]
                      /\ UNCHANGED <<hashchains, replicas, manifests_pending_sync, pending_actions, processing_block, peer_approvals, blocks_request_queues, hashchains_block_uid_set, replicas_block_uid_set, rcvdMsg, prev_block_uid, block_to_send, signoff_peers, actions, next_doc_id, certified_doc_ids>>
                   \/ /\ Len(manifests_pending_sync[self]) > 0
                      /\ with_set' = [with_set EXCEPT ![self] =           {mf_idx \in DOMAIN manifests_pending_sync[self] :
                                                                   replicas[self][manifests_pending_sync[self][mf_idx].block.author] # << >>
                                                                /\ replicas[self][manifests_pending_sync[self][mf_idx].block.author][
                                                                      Len(replicas[self][manifests_pending_sync[self][mf_idx].block.author])].uid
                                                                          = manifests_pending_sync[self][mf_idx].block.parent}]
                      /\ \E mf_idx \in with_set'[self]:
                           /\ Assert(((replicas[self][manifests_pending_sync[self][mf_idx].block.author]) = << >>) <=> ((manifests_pending_sync[self][mf_idx].block).parent = None), 
                                     "Failure of assertion at line 130, column 9 of macro called at line 334, column 17.")
                           /\ Assert((replicas[self][manifests_pending_sync[self][mf_idx].block.author]) = << >> \/ (replicas[self][manifests_pending_sync[self][mf_idx].block.author])[Len((replicas[self][manifests_pending_sync[self][mf_idx].block.author]))].uid = (manifests_pending_sync[self][mf_idx].block).parent, 
                                     "Failure of assertion at line 131, column 9 of macro called at line 334, column 17.")
                           /\ replicas' = [replicas EXCEPT ![self][manifests_pending_sync[self][mf_idx].block.author] = Append((replicas[self][manifests_pending_sync[self][mf_idx].block.author]), (manifests_pending_sync[self][mf_idx].block))]
                           /\ replicas_block_uid_set' = [replicas_block_uid_set EXCEPT ![self][manifests_pending_sync[self][mf_idx].block.author] = (replicas_block_uid_set[self][manifests_pending_sync[self][mf_idx].block.author]) \cup {(manifests_pending_sync[self][mf_idx].block).uid}]
                           /\ manifests_pending_sync' = [manifests_pending_sync EXCEPT ![self] = Remove(mf_idx, manifests_pending_sync[self])]
                      /\ pc' = [pc EXCEPT ![self] = "daemon"]
                      /\ UNCHANGED <<channels, hashchains, sigreqs_pending_sync, pending_actions, processing_block, peer_approvals, blocks_request_queues, hashchains_block_uid_set, rcvdMsg, prev_block_uid, block_to_send, signoff_peers, actions, next_doc_id, certified_doc_ids>>
                   \/ /\ (channels[self]) # <<>>
                      /\ \/ /\ rcvdMsg' = [rcvdMsg EXCEPT ![self] = None]
                            /\ channels' = [channels EXCEPT ![self] = Tail((channels[self]))]
                         \/ /\ rcvdMsg' = [rcvdMsg EXCEPT ![self] = Head((channels[self]))]
                            /\ channels' = [channels EXCEPT ![self] = Tail((channels[self]))]
                         \/ /\ rcvdMsg' = [rcvdMsg EXCEPT ![self] = Head((channels[self]))]
                            /\ UNCHANGED channels
                         \/ /\ rcvdMsg' = [rcvdMsg EXCEPT ![self] = Head((channels[self]))]
                            /\ channels' = [channels EXCEPT ![self] = Append(Tail((channels[self])), Head((channels[self])))]
                         \/ /\ IF Len((channels[self])) >= 2
                                  THEN /\ rcvdMsg' = [rcvdMsg EXCEPT ![self] = (channels[self])[2]]
                                       /\ channels' = [channels EXCEPT ![self] = Append(Tail(Tail((channels[self]))), Head((channels[self])))]
                                  ELSE /\ rcvdMsg' = [rcvdMsg EXCEPT ![self] = None]
                                       /\ UNCHANGED channels
                      /\ pc' = [pc EXCEPT ![self] = "procmsg"]
                      /\ UNCHANGED <<hashchains, replicas, sigreqs_pending_sync, manifests_pending_sync, pending_actions, processing_block, peer_approvals, blocks_request_queues, hashchains_block_uid_set, replicas_block_uid_set, prev_block_uid, block_to_send, signoff_peers, actions, next_doc_id, with_set, certified_doc_ids>>
                /\ UNCHANGED << were_blocks_requested, peer_to_send >>

procmsg(self) == /\ pc[self] = "procmsg"
                 /\ IF rcvdMsg[self] = None
                       THEN /\ TRUE
                            /\ pc' = [pc EXCEPT ![self] = "daemon"]
                            /\ UNCHANGED << channels, replicas, 
                                            manifests_pending_sync, 
                                            pending_actions, processing_block, 
                                            peer_approvals, 
                                            blocks_request_queues, 
                                            replicas_block_uid_set, 
                                            signoff_peers, actions, 
                                            were_blocks_requested >>
                       ELSE /\ IF        rcvdMsg[self].mtype = SignatureRequest
                                  /\ (peer_approvals[self][rcvdMsg[self].mfrom] = PeeringApproved
                                       \/ (peer_approvals[self][rcvdMsg[self].mfrom] = PeeringAwaitingTheirApproval
                                  
                                  
                                            /\ \E action \in rcvdMsg[self].block.actions
                                                    : action.atype = AddPeer /\ action.node = self))
                                  /\ ~(\E b_idx \in DOMAIN replicas[self][rcvdMsg[self].block.author]
                                        : replicas[self][rcvdMsg[self].block.author][b_idx].parent = rcvdMsg[self].block.parent)
                                  /\ ~(\E sr_idx \in DOMAIN sigreqs_pending_sync[self]
                                        : sigreqs_pending_sync[self][sr_idx].block.uid = rcvdMsg[self].block.uid
                                       /\ sigreqs_pending_sync[self][sr_idx].block.author = rcvdMsg[self].block.author)
                                  THEN /\ IF peer_approvals[self][rcvdMsg[self].mfrom] = PeeringAwaitingTheirApproval
                                             THEN /\ peer_approvals' = [peer_approvals EXCEPT ![self][(rcvdMsg[self].mfrom)] = PeeringApproved]
                                                  /\ actions' = [actions EXCEPT ![self] = {[atype |-> AddPeer, node |-> (rcvdMsg[self].mfrom)]}]
                                                  /\ pending_actions' = [pending_actions EXCEPT ![self] = Append(pending_actions[self], actions'[self])]
                                             ELSE /\ TRUE
                                                  /\ UNCHANGED << pending_actions, 
                                                                  peer_approvals, 
                                                                  actions >>
                                       /\ IF ((replicas[self][rcvdMsg[self].mfrom]) = << >> /\ rcvdMsg[self].block.parent # None)
                                             \/ ((replicas[self][rcvdMsg[self].mfrom]) # << >> /\ (replicas[self][rcvdMsg[self].mfrom])[Len((replicas[self][rcvdMsg[self].mfrom]))].uid # rcvdMsg[self].block.parent)
                                             THEN /\ channels' = [channels EXCEPT ![rcvdMsg[self].block.author] = Append((channels[rcvdMsg[self].block.author]), (         [mtype |-> BlocksRequest,
                                                                                                                  mfrom |-> self,
                                                                                                                  after_block_uid |-> IF Len((replicas[self][rcvdMsg[self].mfrom])) = 0 THEN None ELSE (replicas[self][rcvdMsg[self].mfrom])[Len((replicas[self][rcvdMsg[self].mfrom]))].uid,
                                                                                                                  next_block_uid |-> IF Len((replicas[self][rcvdMsg[self].mfrom])) = 0 THEN None ELSE (replicas[self][rcvdMsg[self].mfrom])[Len((replicas[self][rcvdMsg[self].mfrom]))].uid]))]
                                                  /\ were_blocks_requested' = [were_blocks_requested EXCEPT ![self] = TRUE]
                                             ELSE /\ were_blocks_requested' = [were_blocks_requested EXCEPT ![self] = FALSE]
                                                  /\ UNCHANGED channels
                                       /\ pc' = [pc EXCEPT ![self] = "sync_or_sign"]
                                       /\ UNCHANGED << replicas, 
                                                       manifests_pending_sync, 
                                                       processing_block, 
                                                       blocks_request_queues, 
                                                       replicas_block_uid_set, 
                                                       signoff_peers >>
                                  ELSE /\ IF rcvdMsg[self].mtype = SignatureResponse
                                             THEN /\ IF processing_block[self] # None
                                                        /\ rcvdMsg[self].signed_block_uid = processing_block[self].uid
                                                        THEN /\ processing_block' = [processing_block EXCEPT ![self].signatures[rcvdMsg[self].mfrom][2] = TRUE]
                                                        ELSE /\ TRUE
                                                             /\ UNCHANGED processing_block
                                                  /\ UNCHANGED << channels, 
                                                                  replicas, 
                                                                  manifests_pending_sync, 
                                                                  pending_actions, 
                                                                  peer_approvals, 
                                                                  blocks_request_queues, 
                                                                  replicas_block_uid_set, 
                                                                  signoff_peers, 
                                                                  actions, 
                                                                  were_blocks_requested >>
                                             ELSE /\ IF rcvdMsg[self].mtype = PeerRequest
                                                        THEN /\ IF peer_approvals[self][rcvdMsg[self].mfrom] = PeeringNotApproved
                                                                   THEN /\ peer_approvals' = [peer_approvals EXCEPT ![self][rcvdMsg[self].mfrom] = PeeringAwaitingOurApproval]
                                                                        /\ UNCHANGED << channels, 
                                                                                        pending_actions, 
                                                                                        signoff_peers, 
                                                                                        actions >>
                                                                   ELSE /\ IF peer_approvals[self][rcvdMsg[self].mfrom] = PeeringAwaitingTheirApproval
                                                                              THEN /\ peer_approvals' = [peer_approvals EXCEPT ![self][(rcvdMsg[self].mfrom)] = PeeringApproved]
                                                                                   /\ actions' = [actions EXCEPT ![self] = {[atype |-> AddPeer, node |-> (rcvdMsg[self].mfrom)]}]
                                                                                   /\ pending_actions' = [pending_actions EXCEPT ![self] = Append(pending_actions[self], actions'[self])]
                                                                                   /\ UNCHANGED << channels, 
                                                                                                   signoff_peers >>
                                                                              ELSE /\ IF peer_approvals[self][rcvdMsg[self].mfrom] = PeeringApproved
                                                                                         THEN /\ signoff_peers' = [signoff_peers EXCEPT ![self] = ({s \in Nodes : \E b_idx_l \in 1..Len((hashchains[self]))
                                                                                                                                                      : \E a_l \in (hashchains[self])[b_idx_l].actions : a_l.atype = AddPeer /\ a_l.node = s
                                                                                                                                                     /\ \A b_idx_r \in b_idx_l..Len((hashchains[self]))
                                                                                                                                                          : ~(\E a_r \in (hashchains[self])[b_idx_r].actions : a_r.atype = RemovePeer /\ a_r.node = s)}
                                                                                                                                                  \cup
                                                                                                                                                  {s \in Nodes : \E m \in ({}) : m.atype = AddPeer /\ m.node = s})
                                                                                                                                                  \
                                                                                                                                                  {s \in Nodes : \E m \in ({}) : m.atype = RemovePeer /\ m.node = s}]
                                                                                              /\ IF rcvdMsg[self].mfrom \in signoff_peers'[self]
                                                                                                    THEN /\ pending_actions' = [pending_actions EXCEPT ![self] =                      Append(
                                                                                                                                                                 Append(pending_actions[self],
                                                                                                                                                                     {[atype |-> RemovePeer, node |-> rcvdMsg[self].mfrom]}),
                                                                                                                                                                 {[atype |-> AddPeer, node |-> rcvdMsg[self].mfrom]})]
                                                                                                         /\ UNCHANGED << channels, 
                                                                                                                         peer_approvals, 
                                                                                                                         actions >>
                                                                                                    ELSE /\ IF        processing_block[self] # None
                                                                                                               /\ \E a \in processing_block[self].actions : a.atype = AddPeer
                                                                                                                                                         /\ a.node = rcvdMsg[self].mfrom
                                                                                                               THEN /\ channels' = [channels EXCEPT ![rcvdMsg[self].mfrom] = Append((channels[rcvdMsg[self].mfrom]), (       [mtype |-> SignatureRequest,
                                                                                                                                                                             mfrom |-> self,
                                                                                                                                                                             block |-> processing_block[self]]))]
                                                                                                                    /\ UNCHANGED << pending_actions, 
                                                                                                                                    peer_approvals, 
                                                                                                                                    actions >>
                                                                                                               ELSE /\ peer_approvals' = [peer_approvals EXCEPT ![self][(rcvdMsg[self].mfrom)] = PeeringApproved]
                                                                                                                    /\ actions' = [actions EXCEPT ![self] = {[atype |-> AddPeer, node |-> (rcvdMsg[self].mfrom)]}]
                                                                                                                    /\ pending_actions' = [pending_actions EXCEPT ![self] = Append(pending_actions[self], actions'[self])]
                                                                                                                    /\ UNCHANGED channels
                                                                                         ELSE /\ IF peer_approvals[self][rcvdMsg[self].mfrom] = PeeringAwaitingOurApproval
                                                                                                    THEN /\ TRUE
                                                                                                    ELSE /\ TRUE
                                                                                              /\ UNCHANGED << channels, 
                                                                                                              pending_actions, 
                                                                                                              peer_approvals, 
                                                                                                              signoff_peers, 
                                                                                                              actions >>
                                                             /\ UNCHANGED << replicas, 
                                                                             manifests_pending_sync, 
                                                                             blocks_request_queues, 
                                                                             replicas_block_uid_set, 
                                                                             were_blocks_requested >>
                                                        ELSE /\ IF rcvdMsg[self].mtype = BlocksRequest
                                                                   THEN /\ blocks_request_queues' = [blocks_request_queues EXCEPT ![self] = Append(blocks_request_queues[self], rcvdMsg[self])]
                                                                        /\ UNCHANGED << channels, 
                                                                                        replicas, 
                                                                                        manifests_pending_sync, 
                                                                                        replicas_block_uid_set, 
                                                                                        were_blocks_requested >>
                                                                   ELSE /\ IF        rcvdMsg[self].mtype = BlockManifest
                                                                              /\ ~(\E b_idx \in DOMAIN replicas[self][rcvdMsg[self].block.author]
                                                                                    : replicas[self][rcvdMsg[self].block.author][b_idx].uid = rcvdMsg[self].block.uid)
                                                                              /\ ~(\E mf_idx \in DOMAIN manifests_pending_sync[self]
                                                                                    : manifests_pending_sync[self][mf_idx].block.uid = rcvdMsg[self].block.uid
                                                                                   /\ manifests_pending_sync[self][mf_idx].block.author = rcvdMsg[self].block.author)
                                                                              THEN /\ Assert(\A s \in DOMAIN rcvdMsg[self].block.signatures : rcvdMsg[self].block.signatures[s][2], 
                                                                                             "Failure of assertion at line 435, column 13.")
                                                                                   /\ IF ((replicas[self][rcvdMsg[self].block.author]) = << >> /\ rcvdMsg[self].block.parent # None)
                                                                                         \/ ((replicas[self][rcvdMsg[self].block.author]) # << >> /\ (replicas[self][rcvdMsg[self].block.author])[Len((replicas[self][rcvdMsg[self].block.author]))].uid # rcvdMsg[self].block.parent)
                                                                                         THEN /\ channels' = [channels EXCEPT ![rcvdMsg[self].block.author] = Append((channels[rcvdMsg[self].block.author]), (         [mtype |-> BlocksRequest,
                                                                                                                                                              mfrom |-> self,
                                                                                                                                                              after_block_uid |-> IF Len((replicas[self][rcvdMsg[self].block.author])) = 0 THEN None ELSE (replicas[self][rcvdMsg[self].block.author])[Len((replicas[self][rcvdMsg[self].block.author]))].uid,
                                                                                                                                                              next_block_uid |-> IF Len((replicas[self][rcvdMsg[self].block.author])) = 0 THEN None ELSE (replicas[self][rcvdMsg[self].block.author])[Len((replicas[self][rcvdMsg[self].block.author]))].uid]))]
                                                                                              /\ were_blocks_requested' = [were_blocks_requested EXCEPT ![self] = TRUE]
                                                                                         ELSE /\ were_blocks_requested' = [were_blocks_requested EXCEPT ![self] = FALSE]
                                                                                              /\ UNCHANGED channels
                                                                                   /\ IF were_blocks_requested'[self]
                                                                                         THEN /\ manifests_pending_sync' = [manifests_pending_sync EXCEPT ![self] = Append(manifests_pending_sync[self], rcvdMsg[self])]
                                                                                              /\ UNCHANGED << replicas, 
                                                                                                              replicas_block_uid_set >>
                                                                                         ELSE /\ Assert(((replicas[self][rcvdMsg[self].block.author]) = << >>) <=> ((rcvdMsg[self].block).parent = None), 
                                                                                                        "Failure of assertion at line 130, column 9 of macro called at line 440, column 15.")
                                                                                              /\ Assert((replicas[self][rcvdMsg[self].block.author]) = << >> \/ (replicas[self][rcvdMsg[self].block.author])[Len((replicas[self][rcvdMsg[self].block.author]))].uid = (rcvdMsg[self].block).parent, 
                                                                                                        "Failure of assertion at line 131, column 9 of macro called at line 440, column 15.")
                                                                                              /\ replicas' = [replicas EXCEPT ![self][rcvdMsg[self].block.author] = Append((replicas[self][rcvdMsg[self].block.author]), (rcvdMsg[self].block))]
                                                                                              /\ replicas_block_uid_set' = [replicas_block_uid_set EXCEPT ![self][rcvdMsg[self].block.author] = (replicas_block_uid_set[self][rcvdMsg[self].block.author]) \cup {(rcvdMsg[self].block).uid}]
                                                                                              /\ UNCHANGED manifests_pending_sync
                                                                              ELSE /\ TRUE
                                                                                   /\ UNCHANGED << channels, 
                                                                                                   replicas, 
                                                                                                   manifests_pending_sync, 
                                                                                                   replicas_block_uid_set, 
                                                                                                   were_blocks_requested >>
                                                                        /\ UNCHANGED blocks_request_queues
                                                             /\ UNCHANGED << pending_actions, 
                                                                             peer_approvals, 
                                                                             signoff_peers, 
                                                                             actions >>
                                                  /\ UNCHANGED processing_block
                                       /\ pc' = [pc EXCEPT ![self] = "daemon"]
                 /\ UNCHANGED << hashchains, sigreqs_pending_sync, 
                                 hashchains_block_uid_set, rcvdMsg, 
                                 prev_block_uid, block_to_send, next_doc_id, 
                                 with_set, certified_doc_ids, peer_to_send >>

sync_or_sign(self) == /\ pc[self] = "sync_or_sign"
                      /\ IF were_blocks_requested[self]
                            THEN /\ sigreqs_pending_sync' = [sigreqs_pending_sync EXCEPT ![self] = Append(sigreqs_pending_sync[self], rcvdMsg[self])]
                                 /\ UNCHANGED channels
                            ELSE /\ channels' = [channels EXCEPT ![rcvdMsg[self].mfrom] = Append((channels[rcvdMsg[self].mfrom]), (     [mtype |-> SignatureResponse,
                                                                                          mfrom |-> self,
                                                                                          signed_block_uid |-> rcvdMsg[self].block.uid]))]
                                 /\ UNCHANGED sigreqs_pending_sync
                      /\ pc' = [pc EXCEPT ![self] = "daemon"]
                      /\ UNCHANGED << hashchains, replicas, 
                                      manifests_pending_sync, pending_actions, 
                                      processing_block, peer_approvals, 
                                      blocks_request_queues, 
                                      hashchains_block_uid_set, 
                                      replicas_block_uid_set, rcvdMsg, 
                                      prev_block_uid, block_to_send, 
                                      signoff_peers, actions, next_doc_id, 
                                      with_set, were_blocks_requested, 
                                      certified_doc_ids, peer_to_send >>

node(self) == daemon(self) \/ procmsg(self) \/ sync_or_sign(self)

Next == (\E self \in Nodes: node(self))

Spec == Init /\ [][Next]_vars

\* END TRANSLATION

=============================================================================
\* Modification History
\* Last modified Thu Jan 14 08:56:10 PST 2016 by mquinn
\* Last modified Fri Dec 25 19:43:38 UTC 2015 by Administrator
\* Created Fri Dec 18 10:19:23 PST 2015 by mquinn
