use getopts::Options;
use std::env;
use std::fs::File;
use std::path::Path;
use std::error::Error;
use std::{process, u16};
use std::str::FromStr;
use std::io::Read;
use {toml, std};
use key::HSMPublicKey;

#[derive(Debug, Deserialize, Clone)]
pub struct CertChainConfig {
    pub hostname: String,
    pub log_config_filename: String,
    pub port: u16,
    pub rpc_port: u16,
    data_dir: String,
    hsm_pubkey: String,
}

#[derive(Debug, Deserialize, Clone)]
pub struct CertChainConfigNode {
    pub inst_addr: String,
    pub hostname: String,
    pub port: u16,
}

impl CertChainConfig {

    pub fn path_to_data_file(&self, file_name: &str) -> String {
        let node_dir = format!("{}/{}_{}", self.data_dir,
                               self.hostname, self.port);
        // Ensure the node's data directory is created.
        std::fs::create_dir_all(&node_dir).unwrap();
        format!("{}/{}", node_dir, file_name)
    }

    pub fn path_to_data_dir(&self, dir_name: &str) -> String {
        let dir = format!("{}/{}_{}/{}", self.data_dir, self.hostname,
                          self.port, dir_name);
        // Ensure the node's data directory *and* requested directory is created.
        std::fs::create_dir_all(&dir).unwrap();
        dir
    }

    pub fn path_to_hashchain(&self) -> String {
        let dir = format!("{}/{}_{}/hashchains/{}", self.data_dir, self.hostname,
                          self.port, self.hsm_pubkey);
        // Ensure the directory exists.
        std::fs::create_dir_all(&dir).unwrap();
        dir
    }

    pub fn get_hsm_pubkey(&self) -> HSMPublicKey {
        HSMPublicKey::from_string(&self.hsm_pubkey).unwrap()
    }
}

pub fn load() -> Result<CertChainConfig, toml::de::Error> {
    let args: Vec<String> = env::args().collect();
    let program = args[0].clone();

    let mut opts = Options::new();
    opts.optflag("h", "help", "print this help menu");
    opts.optflag("k", "keypair", "print new keypair and exit");
    opts.optopt("c", "config", "set config file path", "FILE");
    opts.optopt("n", "hostname", "set hostname", "HOSTNAME");
    opts.optopt("l", "log_config", "set log config file path", "FILE");
    opts.optopt("p", "port", "set port", "PORT");
    opts.optopt("r", "rpc_port", "set RPC port", "RPC_PORT");
    opts.optopt("d", "data_dir", "set path to data directory", "DATA_DIR_PATH");
    opts.optopt("s", "secret_key", "set secret key", "SECRET_KEY");
    opts.optopt("k", "public_key", "set public key", "PUBLIC_KEY");
    let matches = match opts.parse(&args[1..]) {
        Ok(m) => { m },
        Err(f) => { panic!(f.to_string()) }
    };

    // Display help and exit if -h specified.
    if matches.opt_present("h") {
        print_usage(&program, opts);
        process::exit(0);
    }

    // Print a new keypair and exit if -k specified.
    if matches.opt_present("k") {
        HSMPublicKey::print_new_keypair();
        process::exit(0);
    }

    // Get path to config file and open file for reading.
    match matches.opt_str("c") {
        Some(config_file_path_str) => {
            let config_file_path = Path::new(&config_file_path_str);
            let mut config_file = match File::open(&config_file_path) {
                Err(why) => panic!("Unable to open config file {}: {}",
                                        config_file_path.display(), Error::description(&why)),
                Ok(file) => file
            };

            // Read file contents into string.
            let mut config_file_text = String::new();
            config_file.read_to_string(&mut config_file_text).unwrap();

            // Parse the TOML from the file's string representation.
            toml::from_str(&config_file_text[..])
        },
        None => {
            debug!("No config file provided; will read from cmd line args.");
            let hostname = match matches.opt_str("hostname") {
                Some(h) => { h },
                None => { panic!("Hostname is a required arg."); }
            };
            let log_config_filename = match matches.opt_str("log_config") {
                Some(l) => { l },
                None => { panic!("Log config file is a required arg."); }
            };
            let port = match matches.opt_str("port") {
                Some(p) => {
                    match u16::from_str(&p) {
                        Ok(port) => port,
                        Err(_) => panic!("Invalid port number provided.")
                    }
                },
                None => { panic!("Port is a required arg."); }
            };
            let rpc_port = match matches.opt_str("rpc_port") {
                Some(r) => {
                    match u16::from_str(&r) {
                        Ok(rpc_port) => rpc_port,
                        Err(_) => panic!("Invalid RPC port number provided.")
                    }
                }
                None => { panic!("RPC port is a required arg."); }
            };
            let data_dir = match matches.opt_str("data_dir") {
                Some(d) => { d },
                None => { panic!("Data directory is a required arg."); }
            };
            let hsm_pubkey = match matches.opt_str("hsm_pubkey") {
                Some(pk) => { pk },
                None => { panic!("HSM public key is a required arg."); }
            };

            Ok(CertChainConfig {
                hostname: hostname,
                log_config_filename: log_config_filename,
                port: port,
                rpc_port: rpc_port,
                data_dir: data_dir,
                hsm_pubkey: hsm_pubkey,
            })
        }
    }
}

fn print_usage(program: &str, opts: Options) {
    let brief = format!("Usage: {} [options]", program);
    print!("{}", opts.usage(&brief));
}
