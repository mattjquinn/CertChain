use config::CertChainConfig;
use std::sync::{Arc, RwLock};
use std::thread;
use rpc;
use hashchain::{Hashchain, PendingBlock};
use std::time::{Duration, Instant};
use std::sync::atomic::Ordering;
use chan_signal;

pub fn run(config: CertChainConfig) -> () {
    info!("Starting CertChain daemon.");
    let rpc_interrupt = chan_signal::notify(::OS_SIGNALS_TO_TRAP);

    // Load the node's hashchain (will be created if nonexistent).
    let hashchain = Arc::new(RwLock::new(Hashchain::load(
                config.get_hsm_pubkey(),
                config.path_to_hashchain())));

    // Start the RPC server.
    let hashchain_c1 = hashchain.clone();
    let config_c1 = config.clone();
    let rpc_thread = thread::spawn(move || {
        let mut rpc_listener = match rpc::start(&config_c1, hashchain_c1) {
            Ok(l) => {info!("RPC server started successfully: {:?}", l); l},
            Err(err) => panic!("Failed to start RPC server: {:?}", err)
        };
        rpc_interrupt.recv();
        info!("INTERRUPT recvd by rpc thread, closing listener...");
        match rpc_listener.close() {
            Ok(_) => info!("CLOSED RPC listener successfully."),
            Err(err) => warn!("UNABLE to close RPC listener: {:?}", err)
        }
    });

    // Start the block queue monitor.
    let mut time_since_last_livenessblk: Option<Instant> = None;
    loop {
        if ::SHUTDOWN_SIGNAL_RECVD.load(Ordering::Relaxed) {
            info!("INTERRUPT recvd by FSM, exiting FSM loop.");
            break;
        }
        {
            // This has own scope so write lock is not held while sleeping.
            hashchain.write().unwrap().process_pending_block_queue();
        }
        if time_since_last_livenessblk.is_none()
            || time_since_last_livenessblk.unwrap().elapsed().as_secs() >= 30*60 {
            // Liveness block is written once at startup and every
            // 30 minutes thereafter.
            info!("Queueing liveness block for inclusion in hashchain...");
            hashchain.write().unwrap().queue_pending_block(
                    PendingBlock::liveness());
            time_since_last_livenessblk = Some(Instant::now());
        }
        thread::sleep(Duration::from_millis(1000u64));
    }

    // Wait for all child threads to terminate.
    info!("TERMINATING RPC thread...");
    match rpc_thread.join() {
        Ok(_) =>  info!("TERMINATED RPC thread cleanly."),
        Err(err) => warn!("UNABLE TO TERMINATE RPC thread cleanly; err: {:?}",
                          err)
    }
}
