use std::collections::{LinkedList};
use std::fs;
use serde_json;
use std::io::BufWriter;

pub struct FSM {
    fsm_state_path: String,
    states: LinkedList<FSMState>,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub enum FSMState {
    /*RespondToIdentReq(IdentityRequest),
    ProcessIdentResp(IdentityResponse),
    RequestPeer(InstAddress),
    HandlePeerReq(PeerRequest),
    HandleSigReq(SignatureRequest),
    HandleSigResp(SignatureResponse),
    AddSignatureToProcessingBlock(InstAddress, RecovSignature),
    HandleBlocksReq(BlocksRequest),
    HandleBlockManifest(BlockManifest),*/
}

impl FSM {

    pub fn load(fsm_state_path: String) -> FSM {
        match fs::File::open(&fsm_state_path) {
            Ok(fsm_state_f) => {
                info!("Resuming FSM using state file.");
                FSM {
                    fsm_state_path: fsm_state_path,
                    states: serde_json::from_reader(fsm_state_f).unwrap()
                }
            },
            Err(_) => {
                info!("Creating new FSM, performing initial sync of state file.");
                let fsm = FSM {
                    fsm_state_path: fsm_state_path,
                    states: LinkedList::new()
                };
                fsm.sync_to_disk();
                fsm
            }
        }
    }

    pub fn push_state(&mut self, state: FSMState) {
        self.states.push_back(state);
        self.sync_to_disk();
    }

    pub fn pop_state(&mut self) -> Option<FSMState> {
        let state = self.states.pop_front();
        if state.is_some() {
            self.sync_to_disk();
        }
        state
    }

    fn sync_to_disk(&self) {
        info!("---> START FSM_SYNC -------------->");
        let mut writer = BufWriter::new(fs::File::create(
                &self.fsm_state_path).unwrap());
        serde_json::to_writer_pretty(&mut writer, &self.states).unwrap();
        info!("<--- END FSM_SYNC --------------<");
    }
}
