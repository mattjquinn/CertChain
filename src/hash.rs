use std;
use crypto::sha2::Sha256;
use crypto::digest::Digest;
use std::ops::{Index, Range, RangeFull};
use std::fmt::{Debug, Display, Formatter};
use serde::{ser, de};
use common::ValidityErr;
use rustc_serialize::hex::FromHex;

#[derive(RustcEncodable, RustcDecodable, Copy, Clone, Hash,
         Eq, PartialEq, Ord, PartialOrd)]
pub struct Sha256Hash([u8; 32]);

impl Sha256Hash {
    pub fn blank() -> Sha256Hash {
        Sha256Hash([0u8; 32])
    }

    pub fn hash(data: &[u8]) -> Sha256Hash {
        let Sha256Hash(mut buf) = Sha256Hash::blank();
        let mut sha256 = Sha256::new();
        sha256.input(data);
        sha256.result(&mut buf);
        Sha256Hash(buf)
    }

    /// You should use this function for any string hashes that must
    /// be reproduced in client-side JavaScript; using the other method
    /// that accepts a slice makes the client-side code needlessly complex.
    pub fn hash_string(string: &str) -> Sha256Hash {
        let Sha256Hash(mut buf) = Sha256Hash::blank();
        let mut sha256 = Sha256::new();
        sha256.input_str(string);
        sha256.result(&mut buf);
        Sha256Hash(buf)
    }

    pub fn from_string(hash_str: &str) -> Result<Sha256Hash, ValidityErr> {
        let hash_vec = match hash_str.from_hex() {
            Ok(v) => v,
            Err(_) => return Err(ValidityErr::Sha256HashExpected)
        };

        if hash_vec.len() != 32 {
            return Err(ValidityErr::Sha256HashExpected);
        }

        let mut hash_arr = [0u8; 32];
        for i in 0..hash_vec.len() {
            hash_arr[i] = hash_vec[i];
        }

        Ok(Sha256Hash(hash_arr))
    }

    pub fn from_slice(slice: &[u8]) -> Sha256Hash {
        let Sha256Hash(mut buf) = Sha256Hash::blank();
        assert_eq!(slice.len(), buf.len());
        for i in 0..slice.len() {
            buf[i] = slice[i]
        }
        Sha256Hash(buf)
    }

    pub fn genesis_block_parent_hash() -> Sha256Hash {
        Sha256Hash::from_slice(&[0u8; 32][..])
    }
}

impl Debug for Sha256Hash {
    fn fmt(&self, f: &mut Formatter) -> ::std::fmt::Result {
        let &Sha256Hash(data) = self;
        for b in data.iter() {
            try!(write!(f, "{:02x}", b));
        }
        Ok(())
    }
}

impl ser::Serialize for Sha256Hash {
    fn serialize<S: ser::Serializer>(&self, s: S)
        -> Result<S::Ok, S::Error> {
        s.serialize_str(&format!("{}", self)[..])
    }
}

impl de::Deserialize for Sha256Hash {
    fn deserialize<D: de::Deserializer>(d: D)
            -> Result<Sha256Hash, D::Error> {
        d.deserialize_str(Sha256HashVisitor)
    }
}

struct Sha256HashVisitor;

impl de::Visitor for Sha256HashVisitor {
    type Value = Sha256Hash;

    fn expecting(&self, formatter: &mut Formatter) -> std::fmt::Result {
        write!(formatter, "a Sha256Hash")
    }

    fn visit_str<E: de::Error>(self, value: &str)
            -> Result<Sha256Hash, E> {
        match Sha256Hash::from_string(value) {
            Ok(hash) => Ok(hash),
            Err(_) => Err(de::Error::invalid_value(
                    de::Unexpected::Str(value), &self))
        }
    }
}

impl Display for Sha256Hash {
    fn fmt(&self, f: &mut Formatter) -> ::std::fmt::Result {
        let &Sha256Hash(data) = self;
        for b in data.iter() {
            try!(write!(f, "{:02x}", b));
        }
        Ok(())
    }
}

impl Index<usize> for Sha256Hash {
    type Output = u8;
    fn index(&self, index: usize) -> &u8 {
        let &Sha256Hash(ref data) = self;
        &data[index]
    }
}

impl Index<Range<usize>> for Sha256Hash {
    type Output = [u8];
    fn index(&self, index: Range<usize>) -> &[u8] {
        &self.0[index]
    }
}

impl Index<RangeFull> for Sha256Hash {
    type Output = [u8];
    fn index(&self, _: RangeFull) -> &[u8] {
        &self.0[..]
    }
}
