use hash::Sha256Hash;
use std::collections::{BTreeMap, HashMap, HashSet};
use time;
use signature::RecovSignature;
use key::HSMPublicKey;
use std::{cmp, fs};
use serde_json;
use std::io::BufWriter;
use rand::os::OsRng;
use rand::Rng;

pub type DocumentId = Sha256Hash;
pub type OrganizationName = String;

const HC_SYNC_METADATA: &'static str = "meta.dat";
const HC_SYNC_QPNDBLCK: &'static str = "pending_block_queue.dat";

pub struct Hashchain {
    // The following struct members must be updated on disk upon
    // modification in memory.
    meta: HashchainMetadata,
    pending_block_queue: Vec<PendingBlock>,

    // The following struct members are in-memory only; some may
    // updated, but not persisted to disk.
    hc_dir_path: String,
    block_link_map: HashMap<Sha256Hash, Option<Sha256Hash>>,
    block_height_map: HashMap<u32, Sha256Hash>,
    merkle_tree: MerkleTree,
    org_view: OrganizationView,
}

#[derive(Serialize, Deserialize)]
struct HashchainMetadata {
    hsm_pubkey: HSMPublicKey,
    head_node: Option<Sha256Hash>,
    tail_node: Option<Sha256Hash>,
    processing_block: Option<Block>,
}

#[derive(Serialize, Deserialize)]
struct MerkleTree {
    tree: BTreeMap<DocumentId, MerkleNode>
}

#[derive(Serialize, Deserialize, Clone, Debug)]
struct MerkleNode {
    document_id: DocumentId,
    certified_ts: i64,
    certified_block_height: u32,
    revoked_ts: Option<i64>,
    revoked_block_height: Option<u32>,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct PendingBlock {
    id: u64,
    created_ts: i64,
    created_by: String,
    system_generated: bool,
    submitted: bool,
    description_for_admins: String,
    actions: Vec<Action>,
    admin_sigs: HashMap<String, RecovSignature>,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct PendingBlockSummary {
    id: u64,
    created_ts: i64,
    created_by: String,
    system_generated: bool,
    submitted: bool,
    description_for_admins: String,
    num_certify_actions: u32,
    num_revoke_actions: u32
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Block {
    pub header: BlockHeader,
    actions: Vec<Action>,
    hsm_signature: RecovSignature,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct BlockSummary {
    block_id: Sha256Hash,
    timestamp: i64,
    parent: Sha256Hash,
    num_certify_actions: u32,
    num_revoke_actions: u32
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct BlockHeader {
    timestamp: i64,
    pub parent: Sha256Hash,
    hsm_pubkey: HSMPublicKey,
    merkle_root: Sha256Hash,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Organization {
    pub name: String,
    pub authorized_certifiers: String, // TODO: Make vector.
    pub signing_threshold: String, // TODO: Enumerate this.
}

struct OrganizationView {
    last_applied_block_id: Option<Sha256Hash>,
    active_orgs: HashMap<String, Organization>,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub enum Action {
    Certify(DocumentId),
    Revoke(DocumentId),
    AddOrUpdateOrganization(Organization),
    RemoveOrganization(OrganizationName)
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct DocumentSummary {
    doc_id: DocumentId,
    cert_timestamp: i64,
    rev_timestamp: Option<i64>,
}

#[derive(Debug)]
pub enum AppendErr {
    MissingBlocksSince(Sha256Hash),
    BlockAlreadyInChain,
    BlockParentAlreadyClaimed,
    ChainStateCorrupted,
    AuthorSignatureInvalid,
    MissingPeerSignature,
    InvalidPeerSignature,
    UnexpectedSignoffPeers,
    UnexpectedSignoffPeersHash,
    InvalidActionFound(MerkleTreeErr),
    UnexpectedMerkleRootHash,
}

#[derive(Debug, Eq, PartialEq)]
pub enum RequireAllPeerSigs {
    Yes,
    No
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct DocumentStatusProof {
    contents: String,
    most_recent_block_header: BlockHeader,
    hsm_signature: RecovSignature,
    merkle_node: MerkleNode,
    merkle_proof: MerkleProof,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct MerkleProof {
    branches: Vec<MerkleProofBranch>
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct MerkleProofBranch {
    position: String,
    hash: Sha256Hash,
}

impl HashchainMetadata {

    fn new(hsm_pubkey: HSMPublicKey) -> HashchainMetadata {
        HashchainMetadata {
          hsm_pubkey: hsm_pubkey,
          head_node: None,
          tail_node: None,
          processing_block: None,
        }
    }
}

impl Hashchain {

    /// Given a directory to a hashchain, checks if it exists or needs to
    /// be created. If it exists, it's loaded from file. Otherwise it's
    /// created and initial writes to disk are performed.
    pub fn load(hsm_pubkey: HSMPublicKey, hc_dir_path: String) -> Hashchain {
        match (
               fs::File::open(format!("{}/{}", hc_dir_path, HC_SYNC_METADATA)),
               fs::File::open(format!("{}/{}", hc_dir_path, HC_SYNC_QPNDBLCK)),
               fs::read_dir(format!("{}/blocks", hc_dir_path))) {
            (Ok(meta_f), Ok(pendblks_f), Ok(_)) => {
                info!("Loading existing hashchain for HSM pubkey {:?}.",
                      hsm_pubkey);
                let mut hc = Hashchain {
                    meta: serde_json::from_reader(meta_f).unwrap(),
                    pending_block_queue: serde_json::from_reader(pendblks_f).unwrap(),
                    hc_dir_path: hc_dir_path,
                    block_link_map: HashMap::new(),
                    block_height_map: HashMap::new(),
                    merkle_tree: MerkleTree::new(),
                    org_view: OrganizationView::new(),
                };
                hc.init_internal_structures();
                hc
            },
            _ => {
                info!("Creating new hashchain for HSM pubkey {:?}.", hsm_pubkey);
                let new_hc = Hashchain {
                    meta: HashchainMetadata::new(hsm_pubkey),
                    pending_block_queue: Vec::new(),
                    hc_dir_path: hc_dir_path.clone(),
                    block_link_map: HashMap::new(),
                    block_height_map: HashMap::new(),
                    merkle_tree: MerkleTree::new(),
                    org_view: OrganizationView::new(),
                };
                // Set up directories and perform initial syncs to disk.
                fs::create_dir_all(format!("{}/blocks", hc_dir_path)).unwrap();
                new_hc.sync_to_disk(HC_SYNC_METADATA);
                new_hc.sync_to_disk(HC_SYNC_QPNDBLCK);
                new_hc
            }
        }
    }

    fn init_internal_structures(&mut self) {
        info!("BEGIN Initializing internal hashchain data structures...");
        assert!(self.block_link_map.len() == 0);
        assert!(self.block_height_map.len() == 0);

        // We can't combine the two loops below because one goes backwards
        // (to determine links in chain) and the other goes forward (to
        // determine block height). We cache the blocks here to avoid double
        // reads of block files.
        let mut block_cache = HashMap::new();

        if self.meta.tail_node.is_some() {
            let mut lookup_hash = self.meta.tail_node.unwrap().clone();
            self.block_link_map.insert(lookup_hash, None);
            while lookup_hash != Sha256Hash::blank() {
                let block = self.get_block_by_hash(&lookup_hash).unwrap();
                if block.header.parent != Sha256Hash::blank() {
                    // Index the links between all of the blocks.
                    self.block_link_map.insert(block.header.parent,
                                               Some(block.header.hash()));
                }
                let next_lookup_hash = block.header.parent;
                block_cache.insert(lookup_hash.clone(), block);
                lookup_hash = next_lookup_hash;
            }
        }

        if self.meta.head_node.is_some() {
            let mut lookup_hash = self.meta.head_node;
            let mut height = 1u32;
            while lookup_hash.is_some() {
                // Index each block by its height in the chain.
                self.block_height_map.insert(height, lookup_hash.unwrap());
                // Commit the block to the merkle tree.
                let block = block_cache.get(&lookup_hash.unwrap()).unwrap();
                self.merkle_tree.commit_block(block.clone(), height);
                // Update the organization view.
                self.org_view.apply_block(&block);
                // Prepare for next iteration of loop.
                lookup_hash = self.block_link_map.get(
                    &lookup_hash.unwrap()).unwrap().clone();
                height += 1u32;
            }
        }

        //debug!("HCHAIN Internal structures initialized.");
        //debug!("block_link_map = {:?}", self.block_link_map);
        //debug!("block_height_map = {:?}", self.block_height_map);
        assert!(self.block_link_map.len() == self.block_height_map.len());
        info!("END Initialized internal hashchain data structures.");
    }

    fn sync_to_disk(&self, to_sync: &'static str) {
        info!("---> START HC_SYNC {} for {:?} -------------------->",
            to_sync, self.meta.hsm_pubkey);
        let mut writer = BufWriter::new(fs::File::create(format!("{}/{}",
                self.hc_dir_path, to_sync)).unwrap());
        match to_sync {
            HC_SYNC_METADATA => {
                serde_json::to_writer_pretty(&mut writer, &self.meta).unwrap();
            },
            HC_SYNC_QPNDBLCK => {
                serde_json::to_writer_pretty(&mut writer,
                                             &self.pending_block_queue).unwrap();
            },
            _ => {
                panic!("Unexpected file to sync: {}", to_sync);
            }
        };
        info!("<--- FINISHED HC_SYNC {} for {:?} --------------------<",
            to_sync, self.meta.hsm_pubkey);
    }

    pub fn get_document_status_proof(&self, docid: DocumentId, doc_text: String)
            -> Option<DocumentStatusProof> {
        match self.meta.tail_node {
            None => None,
            Some(hash) => {
                // It's possible that the docid is in a pending block
                // and thus not yet present in the Merkle tree.
                match self.merkle_tree.get_node(docid) {
                    Some(merkle_node) => {
                        let ref block = self.get_block_by_hash(&hash).unwrap();
                        Some(DocumentStatusProof {
                            contents: doc_text,
                            most_recent_block_header: block.header.clone(),
                            hsm_signature: block.hsm_signature.clone(),
                            merkle_node: merkle_node,
                            merkle_proof: self.merkle_tree.merkle_proof(docid),
                        })
                    }, None => None
                }
            }
        }
    }

    pub fn get_active_orgs(&self) -> HashMap<String, Organization> {
        self.org_view.active_orgs.clone()
    }

    /// Retrieves a block from disk by its identifying hash.
    /// TODO: May need to cache frequently accessed blocks in memory down the road.
    pub fn get_block_by_hash(&self, hash: &Sha256Hash) -> Option<Block> {
        match fs::File::open(format!("{}/blocks/{}.blk", self.hc_dir_path, hash)) {
            Ok(block_file) => Some(serde_json::from_reader(block_file).unwrap()),
            Err(_) => None
        }
    }

    /// This method retrieves a block by its block height, because actions
    /// are tied to the block height rather than their block hash. This is
    /// because when a block is authored, its header is based in part
    /// on its merkle root; if we require nodes (actions) in the merkle
    /// tree to refer to the hash of the block currently being constructed,
    /// we get a circular dependency. We break that by using block height in
    /// the action; this is acceptable because our hashchains
    /// are not allowed to branch.
    pub fn get_block_by_height(&self, height: u32) -> Option<Block> {
        match self.block_height_map.get(&height) {
            Some(block_hash) => self.get_block_by_hash(&block_hash),
            None => None
        }
    }

    /// This is the authoritative method for checking whether a given
    /// block can be appended to a hashchain instance, be it an institution's
    /// own hashchain or a replica chain belonging to a peer.
    /// NOTE: This method requires a mutable borrow on self but does not
    /// actually change its state in a way that persists after returning;
    /// the explanation is provided in the documentation for
    /// MerkleTree::compute_root_with_actions.
    pub fn is_block_eligible_for_append(
            &mut self, block: &Block) -> Result<(), AppendErr> {

        // The author of the block must match the author of this chain.
        if block.header.hsm_pubkey != self.meta.hsm_pubkey {
            panic!("Attempted to add block authored by {:?} to chain authored by \
                    {:?}; the caller is doing something wrong.",
                    block.header.hsm_pubkey, self.meta.hsm_pubkey);
        }

        // The author's signature must be valid.
        if block.hsm_signature.check_validity(
                        &block.header.hash(), &block.header.hsm_pubkey).is_err() {
            return Err(AppendErr::AuthorSignatureInvalid);
        }

        // Is this block already present in the chain?
        if self.block_link_map.contains_key(&block.header.hash()) {
            return Err(AppendErr::BlockAlreadyInChain)
        }

        if block.header.parent == Sha256Hash::genesis_block_parent_hash() {
            // If this is the genesis block, ensure our replica state
            // reflects that expectation.
            if self.meta.head_node != None
                    || self.meta.tail_node != None
                    || self.block_link_map.len() > 0 {
                return Err(AppendErr::ChainStateCorrupted)
            }
        } else {
            // Otherwise, if this is not the genesis block, determine
            // if we are missing blocks or if the parent has already been
            // claimed.
            match self.meta.tail_node {
                None => {
                    // If we have nothing in the replica chain and the peer
                    // is sending us a non-genesis block, we have to sync
                    // our replica up starting from the genesis hash (000000...).
                    return Err(AppendErr::MissingBlocksSince(
                            Sha256Hash::genesis_block_parent_hash()))
                },
                Some(tail_hash) => {
                    match self.block_link_map.get(&block.header.parent) {
                        None => {
                            // If we don't have the block's parent, we must
                            // be missing blocks issued since our recorded tail.
                            return Err(AppendErr::MissingBlocksSince(tail_hash));
                        },
                        Some(block_after_parent) => {
                            // Is the parent already claimed?
                            if block_after_parent.is_some() {
                                return Err(AppendErr::BlockParentAlreadyClaimed);
                            }
                            // If the parent is not already claimed, it should
                            // be the tail of our append-only hashchain. Do a
                            // sanity check to be sure.
                            if tail_hash != block.header.parent {
                                return Err(AppendErr::ChainStateCorrupted)
                            }
                        }
                    }
                }
            };
        }

        // Ensure that the block header's merkle root matches what we expect
        // as well; for the same reason as given above for signoff peer
        // verification, it's important to do this after
        // checking for missing blocks.
        let computed_merkle_root =
                match self.merkle_tree.compute_root_with_actions(
            block.header.timestamp, (self.block_link_map.len() as u32) + 1,
                &block.actions) {
            Ok(hash) => hash,
            Err(merkle_err) =>
                return Err(AppendErr::InvalidActionFound(merkle_err))
        };
        if block.header.merkle_root != computed_merkle_root {
            return Err(AppendErr::UnexpectedMerkleRootHash);
        }

        Ok(())
    }

    pub fn append_block(&mut self, block: Block) {

        // Make absolutely sure that this block is eligible
        // to be appended to the chain.
        match self.is_block_eligible_for_append(&block) {
            Ok(_) => {
                let block_header_hash = block.header.hash();

                // Write the block to disk.
                // TODO: Trap interrupts while writing.
                let mut writer = BufWriter::new(fs::File::create(
                        format!("{}/blocks/{}.blk", self.hc_dir_path,
                                block_header_hash)).unwrap());
                serde_json::to_writer_pretty(&mut writer, &block).unwrap();

                // Update chain metadata, with requisite sync to disk.
                self.meta.tail_node = Some(block_header_hash.clone());
                if self.meta.head_node == None {
                    // If this is the first block, adjust the chain head.
                    self.meta.head_node = Some(block_header_hash.clone());
                }
                self.sync_to_disk(HC_SYNC_METADATA);

                // Then update internal data structures.
                self.block_link_map.insert(block.header.parent,
                                           Some(block_header_hash));
                self.block_link_map.insert(block_header_hash.clone(), None);
                let block_height = (self.block_height_map.len() as u32) + 1;
                self.block_height_map.insert(block_height,
                                             block_header_hash.clone());
                self.org_view.apply_block(&block);

                // Commit block actions to internal merkle tree.
                self.merkle_tree.commit_block(block,
                                              self.block_link_map.len() as u32);
                info!("Block successfully appended to chain.");
            },
            Err(err) => {
                panic!("Failed to append block; this should \
                        never happen; reason is: {:?}", err);
            }
        }
    }

    /// Queues a pending block for future inclusion in the hashchain.
    /// We don't create the official block now because we aren't sure what
    /// its parent will be until it's ready for processing.
    pub fn queue_pending_block(&mut self, block: PendingBlock) {
        self.pending_block_queue.push(block);
        self.sync_to_disk(HC_SYNC_QPNDBLCK);
    }

    pub fn discard_pending_block(&mut self, pending_block_id: u64) {
        self.pending_block_queue.retain(|ref b| b.id != pending_block_id)
    }

    pub fn submit_pending_block(&mut self, pending_block_id: u64) {
        for ref mut b in self.pending_block_queue.iter_mut() {
            if b.id == pending_block_id {
                b.submitted = true;
                return;
            }
        }
    }

    /// At any given time, at most one block is processing.
    /// Blocks are processed once they are reached in the queue.
    /// This implies that at any given time, signature requests
    /// are in flight or awaiting receipt for the processing block only;
    /// to leave the processing state, and thus process other blocks in
    /// the queue, we either need all required signatures, or
    /// processing of the block must be aborted.
    /// Returns true if hashchain state was modified (and thus
    /// disk sync required), false otherwise.
    pub fn process_pending_block_queue(&mut self) {
        if self.meta.processing_block.is_none() {
            // Are there any pending blocks ready to be processed?
            let ready_idx= self.pending_block_queue.iter().position(
                |ref b| b.is_ready_for_processing());
            if ready_idx.is_some() {
                let pending_block = self.pending_block_queue.remove(
                    ready_idx.unwrap());
                info!("Creating official block for pending block: {:?}.",
                      pending_block);
                let timestamp = time::get_time().sec;
                // TODO: Handle this rather than unwrapping.
                let merkle_root = match self.merkle_tree
                    .compute_root_with_actions(
                        timestamp, (self.block_link_map.len() as u32) + 1,
                        &pending_block.actions) {
                    Ok(hash) => hash,
                    Err(err) => panic!("Unable to queue \
                                       block; merkle err: {:?}", err)
                };
                let block_to_process = Block::new(self.meta.tail_node,
                   timestamp,
                   self.meta.hsm_pubkey.clone(),
                   merkle_root,
                   pending_block.actions);

                self.meta.processing_block = Some(block_to_process);

                // Hashchain state modified, sync to disk.
                self.sync_to_disk(HC_SYNC_METADATA);
                self.sync_to_disk(HC_SYNC_QPNDBLCK);
            }
        } else {
            let proc_block = self.meta.processing_block.as_ref().unwrap().clone();
            match self.is_block_eligible_for_append(&proc_block) {
                Ok(_) => {
                    info!("HCHAIN: Block is now eligible for append; \
                           adding to chain and broadcasting.");
                    let block = self.meta.processing_block
                        .as_ref().unwrap().clone();
                    self.append_block(block.clone());
                    self.meta.processing_block = None;

                    // Hashchain state modified, sync to disk.
                    self.sync_to_disk(HC_SYNC_METADATA);
                },
                Err(err) => {
                    info!("HCHAIN: Block is not yet \
                          eligible for append: {:?}", err);
                }
            };
        }
    }

    pub fn get_pending_blocks(&self) -> Vec<PendingBlockSummary> {
        self.pending_block_queue.iter().map(|ref pb|
            PendingBlockSummary::from(pb)).collect()
    }

    pub fn get_proc_block_and_latest_n_hcblocks(&self, n: u32)
        -> (Option<BlockSummary>, Vec<BlockSummary>) {
        let mut latest_n_hcblocks = Vec::new();
        let mut lookup_hash = self.meta.head_node;
        let mut i = 0u32;
        while lookup_hash.is_some() && (i <= n) {
            let block = self.get_block_by_hash(&lookup_hash.unwrap()).unwrap();
            latest_n_hcblocks.insert(0, BlockSummary::from(&block));
            lookup_hash = self.block_link_map.get(
                &lookup_hash.unwrap()).unwrap().clone();
            i+=1;
        };
        match self.meta.processing_block {
            None => (None, latest_n_hcblocks),
            Some(ref b) => (Some(BlockSummary::from(b)), latest_n_hcblocks)
        }
    }

    pub fn get_certifications(&self) -> Vec<DocumentSummary> {
        if self.meta.head_node.is_none() { return Vec::new() }
        let mut summaries = HashMap::new();
        let mut lookup_hash = self.meta.head_node;
        // Iterate through the chain from front to back; this is important,
        // because processing revocations depends on the certification
        // being processed beforehand.
        while lookup_hash.is_some() {
            let block = self.get_block_by_hash(&lookup_hash.unwrap()).unwrap();
            for action in block.actions.iter() {
                match *action {
                    Action::Certify(docid) => {
                        summaries.insert(docid,
                            DocumentSummary::new(&block, action.clone()));
                    },
                    Action::Revoke(docid) => {
                        match summaries.get_mut(&docid) {
                            None => {
                                panic!("Encountered revocation of a \
                                    docid that hasn't been certified.");
                            },
                            Some(ref mut summary) => {
                                summary.rev_timestamp = Some(
                                    block.header.timestamp);
                            }
                        }
                    },
                    Action::AddOrUpdateOrganization(_)
                    | Action::RemoveOrganization(_) => {
                        continue;
                    }
                }
            }
            match self.block_link_map.get(&lookup_hash.unwrap()) {
                None => break,
                Some(next_block_hash) => lookup_hash = next_block_hash.clone()
            }
        }

        let mut s_vec : Vec<DocumentSummary> = summaries.iter()
            .map(|(_, s)| s.clone()).collect();
        s_vec.sort();
        s_vec
    }
}

#[derive(Debug)]
pub enum MerkleTreeErr {
    DuplicateCertification,
    RevocationOfNonexistentCertification,
    DuplicateRevocation
}

impl MerkleProof {
    fn new() -> MerkleProof {
        MerkleProof {
            branches: Vec::new()
        }
    }

    fn add_left_branch(&mut self, hash: Sha256Hash) {
        self.branches.push(MerkleProofBranch {
            position: String::from("L"),
            hash: hash
        });
    }

    fn add_right_branch(&mut self, hash: Sha256Hash) {
        self.branches.push(MerkleProofBranch {
            position: String::from("R"),
            hash: hash
        });
    }
}

impl MerkleTree {

    fn new() -> MerkleTree {
        MerkleTree {
            tree: BTreeMap::new(),
        }
    }

    fn get_node(&self, docid: DocumentId) -> Option<MerkleNode> {
        match self.tree.get(&docid) {
            Some(m) => Some(m.clone()),
            None => None
        }
    }

    /// Computes the Merkle root of the tree that would result
    /// after committing the provided block actions. Although this requires
    /// a mutable borrow on this tree, changes made during this
    /// function are reverted before returning. To permanently
    /// apply a block to the tree, you must use commit_block below.
    fn compute_root_with_actions(&mut self,
                               block_ts: i64,
                               block_height: u32,
                               actions: &Vec<Action>)
        -> Result<Sha256Hash, MerkleTreeErr> {
        let mut to_delete = HashSet::new();
        let mut to_revert = HashSet::new();
        for action in actions.iter() {
            match *action {
                Action::AddOrUpdateOrganization(_)
                | Action::RemoveOrganization(_) => continue,
                Action::Certify(docid) => {
                    if self.tree.contains_key(&docid) {
                        return Err(MerkleTreeErr::DuplicateCertification)
                    }
                    to_delete.insert(docid);
                    self.tree.insert(docid, MerkleNode {
                        document_id: docid,
                        certified_ts: block_ts,
                        certified_block_height: block_height,
                        revoked_ts: None,
                        revoked_block_height: None
                    });
                },
                Action::Revoke(docid) => {
                    match self.tree.get_mut(&docid) {
                        None => return Err(
                            MerkleTreeErr::RevocationOfNonexistentCertification),
                        Some(ref mut m_node) => {
                            if m_node.revoked_ts.is_some()
                                || m_node.revoked_block_height.is_some() {
                                return Err(MerkleTreeErr::DuplicateRevocation)
                            }
                            to_revert.insert(docid);
                            m_node.revoked_ts = Some(block_ts);
                            m_node.revoked_block_height = Some(block_height);
                        }
                    }
                }
            }
        }

        let root_with_actions = self.merkle_root();

        // Delete and revert our modifications.
        for docid in to_delete.iter() {
            self.tree.remove(docid);
        }
        for docid in to_revert.iter() {
            let m_node = self.tree.get_mut(&docid).unwrap();
            m_node.revoked_ts = None;
            m_node.revoked_block_height = None;
        }

        Ok(root_with_actions)
    }

    fn merkle_root(&self) -> Sha256Hash {
        fn merkle_root(elements: Vec<Sha256Hash>) -> Sha256Hash {
            if elements.len() == 0 {
                return Sha256Hash::blank()
            }
            if elements.len() == 1 {
                return elements[0]
            }
            let mut parent_row = vec![];
            /*
             * This loop has been adapted from the one used by
             * Andrew Poelstra in his rust-bitcoin project;
             * loops ensures odd-length vecs will have last
             * element duplicated, w/o modification of the vec itself.
             */
            for i in 0..((elements.len() + 1) / 2) {
                let a = elements[i*2];
                let b = elements[cmp::min(i*2 + 1, elements.len() - 1)];
                let combined = a.to_string() + &b.to_string();
                parent_row.push(Sha256Hash::hash_string(&combined))
            }
            merkle_root(parent_row)
        }
        merkle_root(self.tree.iter().map(|(_, m_node)| {
            Sha256Hash::hash_string(&m_node.as_string())
        }).collect())
    }

    fn merkle_proof(&self, docid: DocumentId) -> MerkleProof {
        fn merkle_proof(elements: Vec<Sha256Hash>,
                        hash_to_find: Sha256Hash,
                        proof: &mut MerkleProof) {
            if elements.len() < 2 {
                return
            }
            let mut parent_row = vec![];
            /*
             * This loop has been adapted from the one used by
             * Andrew Poelstra in his rust-bitcoin project;
             * loops ensures odd-length vecs will have last
             * element duplicated, w/o modification of the vec itself.
             */
            let mut new_hash_to_find = None;
            debug!("In this row, hash to find is {:?}", hash_to_find);
            for i in 0..((elements.len() + 1) / 2) {
                let a = elements[i*2];
                let b = elements[cmp::min(i*2 + 1, elements.len() - 1)];
                let combined = a.to_string() + &b.to_string();
                debug!("combined: {}", combined);
                let combined_hash = Sha256Hash::hash_string(&combined);
                debug!("combined hash: {}", combined_hash);
                if a == hash_to_find {
                    proof.add_right_branch(b);
                    new_hash_to_find = Some(combined_hash);
                    debug!("Adding *b* to proof.");
                } else if b == hash_to_find {
                    proof.add_left_branch(a);
                    new_hash_to_find = Some(combined_hash);
                    debug!("Adding *a* to proof.");
                }
                parent_row.push(combined_hash);
            }
            if new_hash_to_find.is_none() {
                panic!("Unable to continue proof; no hash to find next.");
            }
            merkle_proof(parent_row, new_hash_to_find.unwrap(), proof)
        }

        // We need to get the hash of the merkle node corresponding to
        // the requested document ID.
        let docid_merkle_node_hash = Sha256Hash::hash_string(
            &self.tree.get(&docid).unwrap().as_string());
        let mut proof = MerkleProof::new();
        merkle_proof(self.tree.iter().map(|(_, m_node)| {
            let st = &m_node.as_string();
            let hash = Sha256Hash::hash_string(
                &m_node.as_string());
            debug!("m_node: {}, hash: {}", st, hash);
            hash
        }).collect(), docid_merkle_node_hash, &mut proof);
        proof
    }

    /// Commits a block to the tree.
    /// If you only want to check what the merkle root will be if a block
    /// is applied, use compute_root_with_block above.
    fn commit_block(&mut self, block: Block, block_height: u32) {
        // Iterate over the block's actions and commit the
        // document-related ones.
        for action in block.actions.iter() {
            match *action {
                Action::AddOrUpdateOrganization(_)
                | Action::RemoveOrganization(_) => continue,
                Action::Certify(docid) => {
                    self.tree.insert(docid, MerkleNode {
                        document_id: docid,
                        certified_ts: block.header.timestamp,
                        certified_block_height: block_height,
                        revoked_ts: None,
                        revoked_block_height: None
                    });
                },
                Action::Revoke(docid) => {
                    let ref mut m_node = self.tree.get_mut(&docid).unwrap();
                    m_node.revoked_ts = Some(block.header.timestamp);
                    m_node.revoked_block_height = Some(block_height);
                }
            }
        }
    }
}

impl MerkleNode {
    fn as_string(&self) -> String {
        let certified_str = format!("{}|{:?}|{}",
                self.document_id, self.certified_ts,
                self.certified_block_height);
        if self.revoked_ts.is_some() && self.revoked_block_height.is_some() {
            format!("{}|{:?}|{:?}", certified_str, self.revoked_ts.unwrap(),
                    self.revoked_block_height.unwrap())
        } else {
            certified_str
        }
    }
}

impl PendingBlock {
    pub fn new(created_by: String, desc: String, actions: Vec<Action>)
        -> PendingBlock {
        let mut rng = OsRng::new().unwrap();
        PendingBlock {
            id: rng.gen::<u64>(),
            created_ts: time::get_time().sec,
            created_by: created_by,
            system_generated: false,
            submitted: false,
            description_for_admins: desc,
            actions: actions,
            admin_sigs: HashMap::new(),
        }
    }

    pub fn org_management(action: Action) -> PendingBlock {
        PendingBlock {
            id: 0,
            created_ts: time::get_time().sec,
            created_by: String::from("INSTITUTIONAL_ADMIN"),
            system_generated: false,
            submitted: true,
            description_for_admins: String::from("Adding/removing org(s)."),
            actions: vec![action],
            admin_sigs: HashMap::new(),
        }
    }

    pub fn liveness() -> PendingBlock {
        PendingBlock {
            id: 0,
            created_ts: time::get_time().sec,
            created_by: String::from("CERTCHAIN"),
            system_generated: true,
            submitted: true,
            description_for_admins: String::from("Liveness block"),
            actions: Vec::new(),
            admin_sigs: HashMap::new(),
        }
    }

    // TODO: Implement this with actual logic (sigs, submission flag, etc.)
    fn is_ready_for_processing(&self) -> bool {
        return self.submitted
    }
}

impl PendingBlockSummary {
    pub fn from(pending_block: &PendingBlock) -> PendingBlockSummary {
        let (certs, revs) = pending_block.actions.iter().fold((0, 0),
                |(certs, revs), ref action| match action {
                    &&Action::Certify(_) => (certs + 1, revs),
                    &&Action::Revoke(_) => (certs, revs + 1),
                    _ => (certs, revs)
        });
        PendingBlockSummary {
            id: pending_block.id,
            created_ts: pending_block.created_ts,
            created_by: pending_block.created_by.clone(),
            system_generated: pending_block.system_generated,
            submitted: pending_block.submitted,
            description_for_admins: pending_block.description_for_admins.clone(),
            num_certify_actions: certs,
            num_revoke_actions: revs
        }
    }
}

impl Block {
    fn new(parent: Option<Sha256Hash>,
           timestamp: i64,
           hsm_pubkey: HSMPublicKey,
           merkle_root: Sha256Hash,
           actions: Vec<Action>) -> Block {
        let header = BlockHeader::new(timestamp, parent,
                                      hsm_pubkey, merkle_root);
        let header_hash = header.hash();
        Block {
            header: header,
            actions: actions,
            hsm_signature: RecovSignature::mock_hsm_sign(&header_hash, &hsm_pubkey),
        }
    }
}

impl BlockSummary {
    fn from(block: &Block) -> BlockSummary {
        let (certs, revs) = block.actions.iter().fold((0, 0),
                |(certs, revs), ref action| match action {
                    &&Action::Certify(_) => (certs + 1, revs),
                    &&Action::Revoke(_) => (certs, revs + 1),
                    _ => (certs, revs)
        });
        BlockSummary {
            block_id: block.header.hash(),
            timestamp: block.header.timestamp,
            parent: block.header.parent,
            num_certify_actions: certs,
            num_revoke_actions: revs
        }
    }
}

impl BlockHeader {
    pub fn new(timestamp: i64,
               parent: Option<Sha256Hash>,
               hsm_pubkey: HSMPublicKey,
               merkle_root: Sha256Hash) -> BlockHeader {
        let parent_hash = match parent {
            None => Sha256Hash::genesis_block_parent_hash(),
            Some(h) => h,
        };

        BlockHeader {
            timestamp: timestamp,
            parent: parent_hash,
            hsm_pubkey: hsm_pubkey,
            merkle_root: merkle_root
        }
    }

    pub fn hash(&self) -> Sha256Hash {
        /*
         * IMPORTANT: We opt to hash a custom string representation
         * of the block header because we want to be able to reproduce
         * this block header hash in client-side JS. If we depend on rustc
         * serialization, on MsgPack, or on Serde, we will not be able to
         * reproduce this nearly as easily, especially as those libraries
         * change.
         * TODO: Keep this in mind, and continue to add fields of BlockHeader
         * to this hash as they are added during development.
         */
        let to_hash = format!("BLOCKHEADER:{},{},{:?},{}",
                              self.timestamp,
                              self.parent,
                              self.hsm_pubkey,
                              self.merkle_root);
        Sha256Hash::hash_string(&to_hash)
    }
}

impl DocumentSummary {
    fn new(block: &Block, action: Action) -> DocumentSummary {
        match action {
            Action::Certify(doc_id) => {
                DocumentSummary {
                    doc_id: doc_id,
                    cert_timestamp: block.header.timestamp,
                    rev_timestamp: None
                }
            },
            Action::Revoke(_) => {
                panic!("TODO: Handle Revoke in DocumentSummary constructor.");
            },
            a @ Action::AddOrUpdateOrganization(_)
            | a @ Action::RemoveOrganization(_) => {
                panic!("Cannot create DocumentSummary from org mgmt actions;
                        calling code needs to prevent this: {:?}", a)
            }
        }
    }
}

// DocumentSummaries are ordered by cert_timestamp; most
// recent comes before least.
impl Ord for DocumentSummary {
    fn cmp(&self, other: &Self) -> cmp::Ordering {
        other.cert_timestamp.cmp(&self.cert_timestamp)
    }
}
impl PartialOrd for DocumentSummary {
    fn partial_cmp(&self, other: &Self) -> Option<cmp::Ordering> {
        Some(other.cmp(&self))
    }
}
impl PartialEq for DocumentSummary {
    fn eq(&self, other: &Self) -> bool {
        self.cert_timestamp == other.cert_timestamp
    }
}
impl Eq for DocumentSummary {}

impl OrganizationView {

    fn new() -> OrganizationView {
        OrganizationView {
            last_applied_block_id: None,
            active_orgs: HashMap::new()
        }
    }

    fn apply_block(&mut self, block: &Block) {
        // Ensure the block's parent is the last block
        // we've applied.
        match self.last_applied_block_id {
            None => assert!(block.header.parent == Sha256Hash::blank()),
            Some(p) => assert!(block.header.parent == p)
        }

        // Apply any Add/Update/Remove actions in this block
        // to the active organization cache.
        for action in block.actions.iter() {
            match *action {
                Action::Certify(_)
                | Action::Revoke(_) => continue,
                Action::AddOrUpdateOrganization(ref org) => {
                    self.active_orgs.insert(org.name.clone(), org.clone());
                },
                Action::RemoveOrganization(ref org_name) => {
                    self.active_orgs.remove(org_name);
                }
            }
        }

        // Remember this block's id for next time.
        self.last_applied_block_id = Some(block.header.hash());
    }
}
