use secp256k1::key::PublicKey;
use rustc_serialize::hex::{ToHex, FromHex};
use secp256k1::Secp256k1;
use rand::os::OsRng;
use serde::{ser, de};
use std::fmt::{Formatter, Debug, Display};
use std;

const COMPRESSED_PUB_KEY_LEN_BYTES: usize = 33;

#[derive(Clone, Copy)]
pub struct HSMPublicKey {
    secp256k1_pubkey: PublicKey,
}

impl HSMPublicKey {

    pub fn from_secp256k1_pubkey(pubkey : PublicKey) -> HSMPublicKey {
        HSMPublicKey {
            secp256k1_pubkey : pubkey
        }
    }

    pub fn from_string(key_str: &String)
            -> std::io::Result<HSMPublicKey> {
        let key_vec = key_str.to_string().from_hex().unwrap();
        assert!(key_vec.len() == COMPRESSED_PUB_KEY_LEN_BYTES);
        let mut key_arr = [0u8; COMPRESSED_PUB_KEY_LEN_BYTES];
        for i in 0..key_vec.len() {
            key_arr[i] = key_vec[i];
        }
        let context = Secp256k1::new();
        let pub_key = PublicKey::from_slice(&context, &key_arr[..]).unwrap();
        Ok(HSMPublicKey {
            secp256k1_pubkey: pub_key
        })
    }

    pub fn to_string(&self) -> String {
        let context = Secp256k1::new();
        self.secp256k1_pubkey.serialize_vec(&context, true)[..].to_hex()
    }

    pub fn print_new_keypair() {

        let context = Secp256k1::new();
        let mut crypto_rng = OsRng::new().unwrap();

        // A compressed public key is generated here using
        // the true argument to generate_keypair().
        let (priv_key, pub_key) = context.generate_keypair(
            &mut crypto_rng).unwrap();

        println!("Secret key: {:?}", priv_key);
        println!("Compressed public key: {}",
                 &pub_key.serialize_vec(&context, true)[..].to_hex());
    }
}

impl ser::Serialize for HSMPublicKey {
    fn serialize<S: ser::Serializer>(&self, s: S)
            -> Result<S::Ok, S::Error> {
        s.serialize_str(&self.to_string()[..])
    }
}

impl de::Deserialize for HSMPublicKey {
    fn deserialize<D: de::Deserializer>(d: D)
            -> Result<HSMPublicKey, D::Error> {
        d.deserialize_str(HSMPublicKeyVisitor)
    }
}

struct HSMPublicKeyVisitor;

impl de::Visitor for HSMPublicKeyVisitor {
    type Value = HSMPublicKey;

    fn expecting(&self, formatter: &mut Formatter) -> std::fmt::Result {
        write!(formatter, "a HSMPublicKey")
    }

    fn visit_str<E: de::Error>(self, value: &str)
            -> Result<HSMPublicKey, E> {
        match HSMPublicKey::from_string(&value.to_string()) {
            Ok(pk) => Ok(pk),
            Err(_) => Err(de::Error::invalid_value(
                    de::Unexpected::Str(value), &self))
        }
    }
}

impl Debug for HSMPublicKey {
    fn fmt(&self, f: &mut Formatter) -> ::std::fmt::Result {
        try!(write!(f, "{}", self.to_string()));
        Ok(())
    }
}

impl Display for HSMPublicKey {
    fn fmt(&self, f: &mut Formatter) -> ::std::fmt::Result {
        try!(write!(f, "{:?}", self));
        Ok(())
    }
}

impl std::cmp::PartialEq for HSMPublicKey {
    fn eq(&self, other: &HSMPublicKey) -> bool {
        self.secp256k1_pubkey == other.secp256k1_pubkey
    }
}
impl std::cmp::Eq for HSMPublicKey {}
