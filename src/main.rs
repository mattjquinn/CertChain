#[macro_use]
extern crate log;

#[macro_use]
extern crate serde_derive;

extern crate chan;
extern crate serde;
extern crate serde_json;
extern crate log4rs;
extern crate getopts;
extern crate hyper;
extern crate rustc_serialize;
extern crate crypto;
extern crate toml;
extern crate secp256k1;
extern crate rand;
extern crate rust_base58;
extern crate time;
extern crate rmp_serialize as msgpack;
extern crate compress;
extern crate websocket;
extern crate chan_signal;

pub mod common;
pub mod daemon;
pub mod config;
pub mod key;
pub mod hash;
pub mod signature;
pub mod fsm;
pub mod rpc;
pub mod hashchain;

use std::thread;
use std::default::Default;
use chan_signal::Signal;
use std::sync::atomic::{AtomicBool, Ordering, ATOMIC_BOOL_INIT};
use std::process;

pub static OS_SIGNALS_TO_TRAP: &'static [Signal] = &[Signal::INT, Signal::TERM];
pub static SHUTDOWN_SIGNAL_RECVD: AtomicBool = ATOMIC_BOOL_INIT;

fn main() {

    let os_signal = chan_signal::notify(OS_SIGNALS_TO_TRAP);

    // Load configuration settings from file.
    let config = match config::load() {
        Ok(c) => c,
        Err(err) => panic!("Unable to load config file: {:?}", err)
    };

    // Using log4rs as the concrete logging implementation.
    log4rs::init_file(&config.log_config_filename,
                      Default::default()).unwrap();

    // Kick off the main daemon thread.
    info!("Config loaded; spawning daemon thread.");
    let daemon_thread = thread::spawn(move || {
        daemon::run(config);
    });

    info!("Daemon thread spawned.");

    // Wait for shutdown signal; when we get it, set the global
    // atomic shutdown boolean to true and join on the daemon thread.
    let signal = os_signal.recv().unwrap();
    SHUTDOWN_SIGNAL_RECVD.store(true, Ordering::Relaxed);
    warn!("RECEIVED OS signal: {:?}; joining on daemon thread...", signal);
    match daemon_thread.join() {
        Ok(_) => info!("TERMINATED daemon thread cleanly; shutdown complete."),
        Err(err) => warn!("UNABLE TO TERMINATE daemon thread cleanly; err: {:?}",
                          err)
    }
    process::exit(1);
}
