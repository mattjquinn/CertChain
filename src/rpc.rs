use hyper::server::{Server, Request, Listening, Response};
use hyper;
use hyper::net::Fresh;
use hyper::uri::RequestUri::AbsolutePath;
use config::CertChainConfig;
use std::sync::{Arc, RwLock};
use serde_json;
use std::io::Read;
use hash::Sha256Hash;
use std;
use std::fs::File;
use std::io::{Write, BufWriter};
use hashchain::{Action, Organization, Hashchain, PendingBlock};
use std::collections::HashMap;

const RPC_LISTEN : &'static str = "127.0.0.1";

pub fn start(config: &CertChainConfig,
             hashchain: Arc<RwLock<Hashchain>>)
                -> hyper::error::Result<Listening> {
    info!("Starting RPC server on {}:{}...", RPC_LISTEN, config.rpc_port);
    let rpc_server = Server::http((&RPC_LISTEN[..], config.rpc_port)).unwrap();
    // TODO: Save the Listening struct returned here
    // and call close() on it when graceful shutdown is supported.
    let docs_dir = config.path_to_data_dir("documents");
    rpc_server.handle(
        move |mut req: Request, mut res: Response<Fresh>| {
            match (req.method.clone(), req.uri.clone()) {
                (hyper::Post, AbsolutePath(ref path))
                    if path == "/certify" => {
                    let mut req_body = String::new();
                    req.read_to_string(&mut req_body).unwrap();
                    certify(res, hashchain.clone(), &docs_dir, req_body);
                },
                (hyper::Post, AbsolutePath(ref path))
                    if path.len() > 8
                        && &path[0..8] == "/revoke/" => {
                    revoke(res, hashchain.clone(), &path[8..]);
                },
                (hyper::Post, AbsolutePath(ref path))
                    if path.len() > 23
                        && &path[0..23] == "/discard_pending_block/" => {
                    discard_pending_block(res, hashchain.clone(), &path[23..]);
                },
                (hyper::Post, AbsolutePath(ref path))
                    if path.len() > 22
                        && &path[0..22] == "/submit_pending_block/" => {
                    submit_pending_block(res, hashchain.clone(), &path[22..]);
                },
                (hyper::Get, AbsolutePath(ref path))
                    if path == "/organizations" => {
                    handle_organizations_req(res, hashchain.clone());
                },
                (hyper::Get, AbsolutePath(ref path))
                    if path == "/all_certifications" => {
                    handle_all_certifications_req(res, hashchain.clone());
                },
                (hyper::Get, AbsolutePath(ref path))
                    if path == "/pending_blocks" => {
                    handle_pending_blocks_req(res, hashchain.clone());
                },
                (hyper::Get, AbsolutePath(ref path))
                    if path == "/hashchain" => {
                    handle_hashchain_req(res, hashchain.clone());
                },
                (hyper::Get, AbsolutePath(ref path))
                    if path.len() > 30
                        && &path[0..30] == "/certifications_by_student_id/" => {
                    res.send("MQUINN: THIS RPC METHOD NEEDS TO BE REWRITEN \
                        to use on-disk index of documents by student id.\
                         Student id is no longer recorded in chain."
                         .as_bytes()).unwrap();
                },
                (hyper::Get, AbsolutePath(ref path))
                    if path.len() > 10
                        && &path[0..10] == "/document/" => {
                    handle_document_req(res, hashchain.clone(),
                        &docs_dir, &path[10..]);
                },
                (hyper::Get, AbsolutePath(ref path))
                    if path.len() > 17
                        && &path[0..17] == "/block_at_height/" => {
                    get_block_at_height(res, hashchain.clone(), &path[17..]);
                },
                (hyper::Get, AbsolutePath(ref path))
                    if path.len() > 15
                        && &path[0..15] == "/block_by_hash/" => {
                    get_block_by_hash(res, hashchain.clone(), &path[15..]);
                },
                (hyper::Post, AbsolutePath(ref path))
                    if path == "/add_organization" => {
                    let mut req_body = String::new();
                    req.read_to_string(&mut req_body).unwrap();
                    add_organization(res, hashchain.clone(), req_body);
                },
                _ => *res.status_mut() = hyper::NotFound
            }
        }
    )
}

fn certify(res: Response<Fresh>,
           hashchain: Arc<RwLock<Hashchain>>,
           docs_dir_path: &String,
           entries_json: String) {

    debug!("Request to certify: {}", entries_json);
    let mut actions = Vec::new();
    let entries : Vec<HashMap<String, String>> =
        serde_json::from_str(&entries_json).unwrap();
    for entry in entries.iter() {
        let user_id = entry.get("user_id").unwrap();
        let aes256 = entry.get("aes256").unwrap();
        let sha256 = Sha256Hash::from_string(
            &entry.get("sha256").unwrap()).unwrap();

        // Write the document contents to disk for later retrieval.
        let user_dir = format!("{}/{}", docs_dir_path, user_id);
        std::fs::create_dir_all(&user_dir).unwrap();
        let filename = format!("{:?}.txt", &sha256);
        match File::create(format!("{}/{}", &user_dir, &filename)) {
            Ok(file) => {
                let mut writer = BufWriter::new(file);
                match writer.write(&aes256.as_bytes()[..]) {
                    Ok(_) => (),
                    Err(err) => {
                        warn!("Unable to write to document file: {}", err);
                        res.send("Failed to write document contents to disk; \
                                 aborting certification.".as_bytes()).unwrap();
                        return;
                    }
                };
            }, Err(err) => {
                warn!("Unable to create document file: {}", err);
                res.send("Failed to create file to store document ciphertext; \
                         aborting certification.".as_bytes()).unwrap();
                return;
            }
        };

        // Symlink from user directory global documents dir.
        std::os::unix::fs::symlink(format!("{}/{}", &user_id, &filename),
                                   format!("{}/{}", docs_dir_path, &filename))
                                   .unwrap();

        // Save the certification action for queueing below.
        actions.push(Action::Certify(sha256));
    }

    // Queue the actions as a new pending block.
    let ref mut hashchain = *hashchain.write().unwrap();
    hashchain.queue_pending_block(
            PendingBlock::new(String::from("todo:username"),
                              String::from("todo:desc"), actions));
    res.send("OK".as_bytes()).unwrap();
}

fn revoke(res: Response<Fresh>,
                    hashchain: Arc<RwLock<Hashchain>>,
                    docid_param: &str) {
    // Ensure that the provided document ID is valid.
    let docid = match Sha256Hash::from_string(docid_param) {
        Ok(id) => id,
        Err(_) => {
            res.send("Document ID is not valid.".as_bytes()).unwrap();
            return;
        }
    };

    // Create a revocation action for the document.
    let action = Action::Revoke(docid);

    // Have the hashchain queue the revoke action(s) in a new pending block.
    let ref mut hashchain = *hashchain.write().unwrap();
    hashchain.queue_pending_block(
            PendingBlock::new(String::from("todo:username"),
                              String::from("todo:desc"),
                              vec![action]));
    res.send("OK; revocation submitted.".as_bytes()).unwrap();
}

fn discard_pending_block(res: Response<Fresh>,
                         hashchain: Arc<RwLock<Hashchain>>,
                         pending_block_id: &str) {

    let id = match pending_block_id.parse::<u64>() {
        Ok(id) => id,
        Err(_) => {
            res.send("Pending block ID is invalid.".as_bytes()).unwrap();
            return;
        }
    };

    let ref mut hashchain = *hashchain.write().unwrap();
    hashchain.discard_pending_block(id);
    res.send("OK; pending block discarded.".as_bytes()).unwrap();
}

fn submit_pending_block(res: Response<Fresh>,
                        hashchain: Arc<RwLock<Hashchain>>,
                        pending_block_id: &str) {

    let id = match pending_block_id.parse::<u64>() {
        Ok(id) => id,
        Err(_) => {
            res.send("Pending block ID is invalid.".as_bytes()).unwrap();
            return;
        }
    };

    let ref mut hashchain = *hashchain.write().unwrap();
    hashchain.submit_pending_block(id);
    res.send("OK; pending block submitted.".as_bytes()).unwrap();
}

fn handle_organizations_req(res: Response<Fresh>,
                            hashchain: Arc<RwLock<Hashchain>>) {
    let ref hashchain = *hashchain.read().unwrap();
    let json = serde_json::to_string_pretty(
        &hashchain.get_active_orgs()).unwrap();
    res.send(json.as_bytes()).unwrap();
}

fn handle_all_certifications_req(res: Response<Fresh>,
                      hashchain: Arc<RwLock<Hashchain>>) {
    let ref hashchain = *hashchain.read().unwrap();
    let json = serde_json::to_string_pretty(
        &hashchain.get_certifications()).unwrap();
    res.send(json.as_bytes()).unwrap();
}

fn handle_pending_blocks_req(res: Response<Fresh>,
                  hashchain: Arc<RwLock<Hashchain>>) {
    let ref hashchain = *hashchain.read().unwrap();
    let json = serde_json::to_string_pretty(
        &hashchain.get_pending_blocks()).unwrap();
    res.send(json.as_bytes()).unwrap();
}

fn handle_hashchain_req(res: Response<Fresh>,
                        hashchain: Arc<RwLock<Hashchain>>) {
    let ref hashchain = *hashchain.read().unwrap();
    // TODO: Allow client to choose how many recent blocks to load.
    let json = serde_json::to_string_pretty(
        &hashchain.get_proc_block_and_latest_n_hcblocks(10)).unwrap();
    res.send(json.as_bytes()).unwrap();
}

fn handle_document_req(res: Response<Fresh>,
                       hashchain: Arc<RwLock<Hashchain>>,
                       docs_dir_path: &String,
                       doc_id_param: &str) {
    let file_path = format!("{}/{}.txt", docs_dir_path, doc_id_param);
    let mut doc_file = match File::open(&file_path) {
        Ok(file) => file,
        Err(err) => {
            warn!("Unable to open requested document {}; err: {}",
                  doc_id_param, err);
            res.send(format!("Unable to open file: {}", file_path)
                     .as_bytes()).unwrap();
            return;
        }
    };

    let mut doc_text = String::new();
    doc_file.read_to_string(&mut doc_text).unwrap();
    //let doc_contents: Value = serde_json::from_str(&doc_text).unwrap();
    let ref hashchain = *hashchain.read().unwrap();
    match hashchain.get_document_status_proof(
            Sha256Hash::from_string(doc_id_param).unwrap(), doc_text) {
        Some(proof) => res.send(serde_json::to_string_pretty(&proof)
                                .unwrap().as_bytes()),
        None => res.send("A status proof is not yet available for this document; \
                 its containing block may still be processing.".as_bytes())
    }.unwrap();
}

fn get_block_at_height(res: Response<Fresh>,
                       hashchain: Arc<RwLock<Hashchain>>,
                       block_height: &str) {
    let height = match block_height.parse::<u32>() {
        Ok(h) => h,
        Err(_) => {
            res.send("Block height is invalid.".as_bytes()).unwrap();
            return;
        }
    };

    let ref hashchain = *hashchain.read().unwrap();
    match hashchain.get_block_by_height(height) {
        None => {
            res.send("Block does not exist.".as_bytes()).unwrap();
            return;
        },
        Some(b) => {
            let json = serde_json::to_string_pretty(&b).unwrap();
            res.send(json.as_bytes()).unwrap();
        }
    }
}

fn get_block_by_hash(res: Response<Fresh>,
                     hashchain: Arc<RwLock<Hashchain>>,
                     block_hash: &str) {

    let hash = match Sha256Hash::from_string(block_hash) {
        Ok(h) => h,
        Err(_) => {
            res.send("Block hash is not valid.".as_bytes()).unwrap();
            return;
        }
    };

    let ref hashchain = *hashchain.read().unwrap();
    match hashchain.get_block_by_hash(&hash) {
        None => {
            res.send("Block does not exist.".as_bytes()).unwrap();
            return;
        },
        Some(b) => {
            let json = serde_json::to_string_pretty(&b).unwrap();
            res.send(json.as_bytes()).unwrap();
        }
    }
}

fn add_organization(res: Response<Fresh>,
                    hashchain: Arc<RwLock<Hashchain>>,
                    org_json: String) {
    debug!("Request to add organization: {}", org_json);
    let entry: HashMap<String, String> =
        serde_json::from_str(&org_json).unwrap();
    let org = Organization {
        name: entry.get("org_name").unwrap().clone(),
        authorized_certifiers: entry.get("authorized_certifiers").unwrap().clone(),
        signing_threshold: entry.get("signing_threshold").unwrap().clone(),
    };

    // Queue the action as a new pending block.
    let ref mut hashchain = *hashchain.write().unwrap();
    hashchain.queue_pending_block(
            PendingBlock::org_management(Action::AddOrUpdateOrganization(org)));
    res.send("OK".as_bytes()).unwrap();
}
