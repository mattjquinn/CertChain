use std;
use rustc_serialize::{Encodable, Decodable, Encoder, Decoder};
use std::fmt::{Formatter, Debug, Display};
use secp256k1::{RecoverableSignature, Secp256k1, RecoveryId, Message};
use secp256k1::key::SecretKey;
use common::ValidityErr;
use hash::Sha256Hash;
use serde::{ser, de};
use rustc_serialize::hex::{ToHex, FromHex};
use key::HSMPublicKey;

#[derive(Clone)]
pub struct RecovSignature {
    ctx: Secp256k1,
    sig: RecoverableSignature,
}

impl RecovSignature {

    /**
     * TODO: IMPORTANT: This is a mock HSM stub method. This will need
     * to be replaced with an API call to an actual HSM.
     */
    pub fn mock_hsm_sign(hash: &Sha256Hash,
                         hsm_pubkey : &HSMPublicKey) -> RecovSignature {
        const SECRET_KEY_LEN_BYTES: usize = 32;

        // Hard-coding PK -> SK mappings, as an HSM would do.
        let sk_str = match &hsm_pubkey.to_string()[..] {
            "02548f17a2f18fce94c50e5a795a9f49ed7f34aaa82a96afa5adc1bffed698db8f" =>
                "eb1bcbd2d5bf58dbf70440b278dd71dfb6ba7e7eeed395f54ad1861d342d54d9",
            "02c3da0eda621bfca008785717d120ed098fe4da28bbb60dde10d7917487ed7055" =>
                "69fd6f867235df9bef8c0d5929b2be3c68816cf9f2d4ff27d890afdc5cc44be7",
            "033184898d42d3f4ffe17e9ebaafaccfa80c2408f43fe33725c22334e73ffe63a3" =>
                "5221da6630a974c34fb4fbc1fd4ef8085142a92b185374b7da58798604619aaf",
            "021d6377b906cfb6881f808f0911dbc7eb82a0898d20a18a5a7dc463ee3283fa11" =>
                "7c90dd25291e8beee4fb2e7e534a6b681805cbddb8d1e7870ece36493ea38de3",
            "02d0e34719da797ae105ae0d534700224f0c30a2629ff0e926060d7d0219494285" =>
                "0cc0a88f0de10facdad89efbf5f9351ffefb938088a89dbe5c9fa0f088bba1a2",
            "03c15abe735e7407a0d3214844fdce104b04d27e403255250c90c17197ae1b56eb" =>
                "bb413662c979a6258c953be2ea296c272e0cb75ba61dea9f1fbf4c55cc954166",
            _ => panic!("Mock HSM does not have a secret key mapped to pk: {}",
                        hsm_pubkey)
        };

        // Prepare to convert the secret key from string to secp256k1 object.
        let key_vec = sk_str.to_string().from_hex().unwrap();
        assert!(key_vec.len() == SECRET_KEY_LEN_BYTES);
        let mut key_arr = [0u8; SECRET_KEY_LEN_BYTES];
        for i in 0..key_vec.len() {
            key_arr[i] = key_vec[i];
        }

        let ctx = Secp256k1::new();
        let secret_key = SecretKey::from_slice(&ctx, &key_arr[..]).unwrap();
        let sig = ctx.sign_recoverable(&Message::from_slice(&hash[..]).unwrap(),
            &secret_key).unwrap();
        RecovSignature {
            ctx: ctx,
            sig: sig
        }
    }

    /// Determines if this RecovSignature object is a signature for
    /// @expected_msg using @hsm_pubkey.
    pub fn check_validity(&self, expected_msg: &Sha256Hash,
                          hsm_pubkey: &HSMPublicKey) -> Result<(), ValidityErr> {
        // First, recover the public key that yields this RecovSignature
        // for expected_msg.
        let msg = match Message::from_slice(&expected_msg[..]) {
            Ok(msg) => msg,
            Err(_) => return Err(ValidityErr::Secp256k1MessageInvalidErr)
        };
        let recov_pubkey = match self.ctx.recover(&msg, &self.sig) {
            Ok(pubkey) => HSMPublicKey::from_secp256k1_pubkey(pubkey),
            Err(_) => return Err(ValidityErr::Secp256k1PubkeyRecoveryErr)
        };

        // Then, compare the provided pubkey with the recovered one. If they
        // match, the signature is valid for @expected_msg.
        if *hsm_pubkey != recov_pubkey {
            return Err(ValidityErr::RecoveredFromAddrDoesntMatch)
        }
        Ok(())
    }

    pub fn from_string(sig_str: &str) -> Result<RecovSignature, ValidityErr> {

        if sig_str.len() < 3 {
            return Err(ValidityErr::RecovSignatureExpected);
        }

        let recid = match (&sig_str[0..1]).parse::<i32>() {
            Ok(r) => {
                match RecoveryId::from_i32(r) {
                    Ok(id) => id,
                    Err(_) => return Err(ValidityErr::RecovSignatureExpected)
                }
            }
            Err(_) => return Err(ValidityErr::RecovSignatureExpected)
        };

        let sig_vec = match (&sig_str[2..]).from_hex() {
            Ok(v) => v,
            Err(_) => return Err(ValidityErr::RecovSignatureExpected)
        };

        let ctx = Secp256k1::new();
        let sig = RecoverableSignature::from_compact(
            &ctx, &sig_vec, recid).unwrap();
        Ok(RecovSignature {
            ctx: ctx,
            sig: sig,
        })
    }
}

impl ser::Serialize for RecovSignature {
    fn serialize<S: ser::Serializer>(&self, s: S)
            -> Result<S::Ok, S::Error> {
        s.serialize_str(&format!("{}", self)[..])
    }
}

impl de::Deserialize for RecovSignature {
    fn deserialize<D: de::Deserializer>(d: D)
            -> Result<RecovSignature, D::Error> {
        d.deserialize_str(RecovSignatureVisitor)
    }
}

struct RecovSignatureVisitor;

impl de::Visitor for RecovSignatureVisitor {
    type Value = RecovSignature;

    fn expecting(&self, formatter: &mut Formatter) -> std::fmt::Result {
        write!(formatter, "a RecovSignature")
    }

    fn visit_str<E: de::Error>(self, value: &str)
            -> Result<RecovSignature, E> {
        match RecovSignature::from_string(value) {
            Ok(sig) => Ok(sig),
            Err(_) => Err(de::Error::invalid_value(
                    de::Unexpected::Str(value), &self))
        }
    }
}

impl Encodable for RecovSignature {
    // TODO: This should use the emit_struct, et al methods so that
    // this can be compatible with both MessagePack for net serialization
    // *and* JSON for RPC HTTP responses.
    fn encode<S: Encoder>(&self, s: &mut S) -> Result<(), S::Error> {
        let (recid, bytes) = self.sig.serialize_compact(&self.ctx);
        try!(recid.to_i32().encode(s));
        for b in bytes.iter() {
            try!(b.encode(s));
        }
        Ok(())
    }
}

impl Decodable for RecovSignature {
    // TODO: See note above for Encodable impl.
    fn decode<D: Decoder>(d: &mut D) -> Result<RecovSignature, D::Error> {
        let ctx = Secp256k1::new();
        let recid = RecoveryId::from_i32(try!(<i32>::decode(d))).unwrap();
        let mut bytes = [0u8; 64];
        for i in 0..bytes.len() {
            bytes[i] = try!(<u8>::decode(d));
        }
        let sig = RecoverableSignature::from_compact(&ctx, &bytes, recid).unwrap();
        Ok(RecovSignature {
            ctx: ctx,
            sig: sig,
        })
    }
}

impl Debug for RecovSignature {
    fn fmt(&self, f: &mut Formatter) -> ::std::fmt::Result {
        try!(write!(f, "{}", self));
        Ok(())
    }
}

impl Display for RecovSignature {
    fn fmt(&self, f: &mut Formatter) -> ::std::fmt::Result {
        let (recid, bytes) = self.sig.serialize_compact(&self.ctx);
        try!(write!(f, "{}|{}", recid.to_i32(), &bytes[..].to_hex()));
        Ok(())
    }
}
