/*
 * TODO: On www, put a note about HSM for managed so I remember to say I support this.
 * TODO: Certify b/w 1 and 10 certs on every cert action.
 * TODO: Revoke b/w 1 and 10 certs on every revoke action.
 * TODO: Add ability to replay a harness log to replicate tricky situations.
 * TODO:
 *  - Detect existing logs/data_dirs automatically, archive to timestamped dir.
 *  - Start with single-node setup: allow certification, etc.
 *  - Then move to two-node setup and allow peering initiation/approval.
 *    - NOTE: Need an FSM per node instance.
 * FSM Ideas:
 *  - ACTION: Create an ephemeral instance that uses a random keypair.
 *  - ACTION: Create actions that use netem (tc) to change network conditions.
 */


const assert = require('assert');
const fs = require('fs');
const cp = require('child_process');
const request = require("request");
const random = require("random-js")();
const http = require('http');
const uuid = require('uuid');
const sha256 = require('sha256');
const aws = require('aws-sdk');

aws.config.loadFromPath('./aws_sns_credentials.json');
const aws_sns = new aws.SNS();
const PUBLISH_SNS_ALERT_ON_FAILURE = true

// Reference console.log in a new variable and nullify console.log
// to ensure we don't accidentally use it later.
var console_logger = console.log;
console.log = null;
// Tee messages to console and file.
const harness_log = function(msg) {
  console_logger(msg);
  fs.appendFile(`logs/harness.log`, msg+'\n', function(err) {
    assert(!err, `Error appending to harness.log: ${err}.`);
  });
};

harness_log(`Testing harness starting up...`);

// All child processes must have the secp256k1 library
// in their library path.
process.env.LD_LIBRARY_PATH = '../secp256k1_server_libs/lib';
process.env.RUST_BACKTRACE = '1';

// Various constants:
const CC_PROC_HOSTNAME = '127.0.0.1';
const CC_PROC_BASE_NET_PORT = 5000;
const CC_PROC_BASE_RPC_PORT = 6000;
const CC_PROC_DATA_DIR_PATH = 'data_dirs';
const CC_PROC_LOG_CONFIG_PATH = './cc_node_harness_log_config.toml';

nodes = {
  1: {
     'pk': '03dea07215fd1b0832bde093ab80a3e41aadab73c8eb2baa7092ba9b89b571ba77',
     'sk': '6cd0c7324c78dca8c8b040a627aae68b6a5bff67df6986f5eb3e58a7fafaddd1',
     'state': 'STOPPED', // STOPPED, STARTING, STARTED, STOPPING
     'cc_proc': null,
     'certifying': [],
     'num_certified': 0,
  },
};

var filter_certified_documents = function(node_id, c_id) {
    harness_log(`QUERYING pending cert ${c_id} on node${node_id}.`);
    if (c_id >= nodes[node_id].certifying.length) {
      // If we've reached the end, filter all the confirmed certs out.
      var pre_filter = nodes[node_id].certifying.length;
      nodes[node_id].certifying = nodes[node_id].certifying.filter(function(c) {
        return !c.confirmed;
      });
      var post_filter = nodes[node_id].certifying.length;
      var filtered = pre_filter - post_filter;
      nodes[node_id].num_certified += filtered;
      harness_log(`FILTERED ${filtered} confirmed certs for node${node_id}; `
              + `total certified: ${nodes[node_id].num_certified}; `
              + `still unconfirmed: ${JSON.stringify(nodes[node_id].certifying)}`);
      nodes[node_id].state = 'STARTED';
      harness_log(`STARTED node${node_id}`);
      return;
    }
    c = nodes[node_id].certifying[c_id];
    rpc_get(node_id, '/document/'+c.doc_hash, function(resp_data) {
      try {
        doc_struct = JSON.parse(resp_data);
        if (doc_struct.merkle_node.certified_ts != null) {
          c.confirmed = true;
          harness_log(`CERTIFIED ${c.doc_hash.substring(0, 10)}... `
                      + `on node${node_id}.`);
        } else {
          assert((new Date().getTime() - c.ts) < 60*10, `The max ` +
                  `time to certify a document has been exceeded for ` +
                  `node${node_id} having document with hash ${c.doc_hash}`);
        }
      } catch(err) {
        // If we're here, ensure it's b/c proof is not yet available.
        // This is not a critical error since we can re-check later.
        assert(resp_data.startsWith(
                'A status proof is not yet available'),
                `Expected cause of err:${err} to be proof not yet `
               +`available, but response is actually: ${resp_data}.`);
      }
      filter_certified_documents(node_id, c_id+1);
    }, function(err) {
       assert(false, `RPC filter certified confirmation request to `
               + `node${node_id} failed; network error is: ${err.message}.`);
    });
};

// Starts a node from the node table above.
var start_node = function(node_id) {
  assert(nodes[node_id].state == 'STOPPED',
          'Caller attempted to start a node that is already started.');
  nodes[node_id].state = 'STARTING';
  net_port = CC_PROC_BASE_NET_PORT + parseInt(node_id);
  rpc_port = CC_PROC_BASE_RPC_PORT + parseInt(node_id);
  const cc_proc = cp.spawn('../target/debug/certchain',
                  [`--hostname=${CC_PROC_HOSTNAME}`,
                   `--port=${net_port}`,
                   `--rpc_port=${rpc_port}`,
                   `--data_dir=${CC_PROC_DATA_DIR_PATH}`,
                   `--secret_key=${nodes[node_id]['sk']}`,
                   `--public_key=${nodes[node_id]['pk']}`,
                   `--log_config=${CC_PROC_LOG_CONFIG_PATH}`]);
  nodes[node_id].cc_proc = cc_proc;

  cc_proc.on('error', (err) => {
    assert(false, `Unable to start node ${node_id} due to err: ${err}`);
  });
  cc_proc.stdout.on('data', (data) => {
    fs.appendFile(`logs/node${node_id}.log`, `STDOUT:${data}`, function(err) {
      assert(!err, `Error appending STDOUT to node${node_id}.log: ${err}.`);
    });
  });
  cc_proc.stderr.on('data', (data) => {
    fs.appendFile(`logs/node${node_id}.log`, `STDERR:${data}`, function(err) {
      assert(!err, `Error appending STDERR to node${node_id}.log: ${err}.`);
    });
    setTimeout(function(){
      assert(false, `Node${node_id} wrote to STDERR a few seconds ago, ` +
              `must fix issue.`);
    }, 5000);
  });
  cc_proc.on('exit', (code, sig) => {
    fs.appendFile(`logs/node${node_id}.log`,
            `EXIT:Process exited with code:${code} and sig:${sig}.\n`,
            function(err) {
      assert(!err, `Error appending EXIT to node${node_id}.log: ${err}.`);
      // Nodes should never exit on their own, but this harness may kill them.
      // MQUINN 01-09-2016: I think this check is generating false positives
      // due to delayed calling of this callback. Disabling for now; we'll be
      // able to detect incorrectly failed nodes when they don't respond to
      // us in other actions.
      //assert(nodes[node_id].state == 'STOPPING'
      //        || nodes[node_id].state == 'STOPPED',
      //        `Node${node_id} exited unexpectedly with code:${code} `
      //        + `and signal:${sig}.`);
    });
  });

  // Only register the child process as started when we can reach its RPC server.
  const wait_for_rpc = function(attempt) {
    harness_log(`WAITING for node${node_id} RPC server (attempt ${attempt}).`);
    assert(attempt <= 10, `Max attempts to reach RPC server while starting `
                            + `node${node_id} exceeded.`);
    rpc_get(node_id, '/network', function(resp_data) {
      try {
        var resp = JSON.parse(resp_data);
        assert(resp.our_hostname == CC_PROC_HOSTNAME,
                `Unexpected hostname: ${resp.our_hostname}`);
        assert(resp.our_port == net_port,
                `Unexpected net port: ${resp.our_port}`);

        // Now that the node is started, figure out which certified
        // documents we still have left to certify.
        filter_certified_documents(node_id, 0);
      } catch(err) {}
    }, function(err) {
      setTimeout(function(){wait_for_rpc(attempt+1);}, 2000);
    });
  };
  wait_for_rpc(1);
};

const rpc_post = function(node_id, rpc_route, post_data, resp_callback) {
  var post_options = {
    method: 'POST',
    host: CC_PROC_HOSTNAME, port: CC_PROC_BASE_RPC_PORT + parseInt(node_id),
    path: rpc_route,
    headers: {'Content-Type': 'application/json'}
  };
  var post_request = http.request(post_options, function(res) {
    res.setEncoding('utf8');
    res.on('data', function(resp_data) {
      resp_callback(resp_data);
    });
  });
  post_request.on('error', function(e) {
    assert(false, 'Error from RPC POST: ' + e.message);
  });
  post_request.write(post_data);
  post_request.end();
};

const rpc_get = function(node_id, rpc_route, resp_callback, err_callback) {
  var get_options = {
    method: 'GET',
    host: CC_PROC_HOSTNAME, port: CC_PROC_BASE_RPC_PORT + parseInt(node_id),
    path: rpc_route
  };
  var get_request = http.request(get_options, function(res) {
    res.setEncoding('utf8');
    res.on('data', function(resp_data) {
      resp_callback(resp_data);
    });
  });
  get_request.on('error', function(err) {
    err_callback(err);
  });
  get_request.end();
};

// Initially, start all nodes.
Object.keys(nodes).forEach(function(node_id) {
  harness_log(`INITIAL START of node${node_id}.`);
  start_node(node_id);
});

const STARTUP_ACTION = 0;
const CERTIFY_ACTION = 1;
const SHUTDOWN_ACTION = 2;

const FSM_MIN_WAIT = 2000;
const FSM_MAX_WAIT = 4000;

const fsm = function() {
  var next_action = random.integer(0,2);

  switch (next_action) {
    case STARTUP_ACTION: {
      // ================================================================
      // Pick a random node; if it's stopped, start it.
      // ================================================================
      var node_id = random.integer(1, Object.keys(nodes).length);
      if (nodes[node_id].state == 'STOPPED') {
          harness_log(`STARTING node${node_id}.`);
          start_node(node_id);
      }
      setTimeout(() => {fsm();}, random.integer(FSM_MIN_WAIT,FSM_MAX_WAIT));
      break;
    } case CERTIFY_ACTION: {
      // ================================================================
      // Pick a random node; if it's started, certify a new document.
      // ================================================================
      node_id = random.integer(1, Object.keys(nodes).length);
      if (nodes[node_id].state == 'STARTED') {
        var doc_pt = uuid.v4();
        var doc_hash = sha256(doc_pt);
        var to_certify = '["{\\"user_id\\":\\"' + random.integer(1,5) +
              '\\",\\"aes256_ct\\":\\"' + doc_pt + '\\",\\"sha256_pt\\":\\"' +
                      doc_hash + '\\"}"]'
        harness_log(`ISSUING CERTIFY FOR ${doc_hash.substring(0, 10)}... `
                      + `on node${node_id}.`);
        rpc_post(node_id, '/certify', to_certify, function(resp_data) {
          assert(resp_data == 'OK', 'Expected RPC server to OK our certify request'
              + ' but got this response: ' + resp_data);
          harness_log(`CERTIFYING ${doc_hash.substring(0, 10)}... `
                      + `on node${node_id}.`);
          nodes[node_id].certifying.push({'doc_hash': doc_hash,
                  'ts': new Date().getTime(),'confirmed':false});
        });
      }
      setTimeout(() => {fsm();}, random.integer(FSM_MIN_WAIT,FSM_MAX_WAIT));
      break;
    } case SHUTDOWN_ACTION: {
      // ================================================================
      // Pick a random node; if it's started, stop it.
      // ================================================================
      if (random.integer(1,4) == 4) {
        // Make this 25% chance to cut down on frequent reboots.
        var node_id = random.integer(1, Object.keys(nodes).length);
        if (nodes[node_id].state == 'STARTED') {
            nodes[node_id].state = 'STOPPING';
            harness_log(`STOPPING node${node_id}.`);
            nodes[node_id].cc_proc.on('exit', (code, sig) => {
              nodes[node_id].cc_proc = null;
              nodes[node_id].state = 'STOPPED';
              harness_log(`STOPPED node${node_id}.`);
              // We only want to advance the FSM when we're sure the
              // node has terminated completely.
              setTimeout(() => {fsm();}, random.integer(FSM_MIN_WAIT,FSM_MAX_WAIT));
            });
            nodes[node_id].cc_proc.kill();
        } else {
          setTimeout(() => {fsm();}, random.integer(FSM_MIN_WAIT,FSM_MAX_WAIT));
        }
      } else {
        setTimeout(() => {fsm();}, random.integer(FSM_MIN_WAIT,FSM_MAX_WAIT));
      }
      break;
    } default: {
      throw new Error('Action ' + next_action + ' is not defined.');
    }
  }
};

fsm();

// Clean up resources before harness exits.
has_already_published_sns_alert = false;
process.on('uncaughtException', function(err){
  harness_log(err.message);
  harness_log(err.stack);
  Object.keys(nodes).forEach(function(k) {
    if (nodes[k].cc_proc != null) {
      try {
        harness_log(`CLEANUP killing node${k}.`);
        nodes[k].cc_proc.kill();
      } catch (killerr) {
        harness_log(`CLEANUP was unable to kill node${k}: ${killerr}`);
      }
    }
  });

  if (PUBLISH_SNS_ALERT_ON_FAILURE) {
    if (!has_already_published_sns_alert) {
      aws_sns.publish({
          Message: `Exception: ${err.message}. Stack trace: ${err.stack}.`,
          Subject: '[Failure] CertChain Test Harness',
          TopicArn: 'arn:aws:sns:ap-south-1:800371459836:CertChainTestHarnessFailure'
        }, function(err, data) {
          if (err) {
            harness_log(`SNS: Unable to publish exception notice: ${err}.`, err);
          } else {
            harness_log(`SNS: Successfully published exception notice.`);
          }
          process.exit(1);
        });
     } else {
       harness_log(`SNS: Already published exception notice, not publishing again.`);
     }
   } else {
     harness_log(`SNS: Not publishing exception notice, flag not set.`);
     process.exit(1);
   }
});
