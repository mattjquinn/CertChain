$('document').ready(function(){

  var viewModel = {
    csvInProgress: ko.observable(false),
    csvTotalCertifications: ko.observable(),
    csvToSendCertChain: ko.observable(),
    csvSubmitForm: function() {
      // TODO: Save keys to local storage.
      return true; // Submit as usual HTML form.
    },
    csvCancelSubmission: function() {
      $('#csv_file_upload').val('');
      viewModel.csvToSendCertChain(null);
    }
  };

  if (window.File && window.FileReader && window.FileList && window.Blob) {
    function handleFileSelect(evt) {      
      viewModel.csvInProgress(true);

      // TODO: Prepend a version so we can retain backwards compat.
      // TODO: Make generic for handling any future document type.
      var csv_to_document = function(doc_type, csv_arr) {
        if (doc_type=='DIPLOMA') {
          return JSON.stringify({
            'certchain_header': {
              'version': '1.0',
              'random': forge.util.bytesToHex(forge.random.getBytesSync(16))
            },
            'document': {
              'type': doc_type,
              'name': csv_arr[2],
              'subject': csv_arr[3],
              'conferral_date': csv_arr[4]
            }
          });
        } else {
          alert('UNSUPPORTED DOC TYPE');
        }
      };

      if (evt.target.files.length == 1) {
        var reader = new FileReader();
        reader.onload = (function(theFile) {
          return function(e) {
            var key_arr = [];
            var cc_arr = [];

            var lines = e.target.result.split('\n');
            lines.forEach(function(line, i){
              var csv_arr = line.split(',');
              if (csv_arr.length != 5) { return true; }
              var doc_pt = csv_to_document('DIPLOMA', csv_arr);

              // Hash plaintext.
              var md = forge.md.sha256.create();
              md.update(doc_pt);
              var sha256 = md.digest().toHex();

              // Encrypt plaintext.
              var aes_iv = forge.random.getBytesSync(16);
              var aes_key = forge.random.getBytesSync(16);
              var cipher = forge.cipher.createCipher('AES-CBC', aes_key);
              cipher.start({iv: aes_iv});
              cipher.update(forge.util.createBuffer(doc_pt));
              cipher.finish();
              var aes256 = cipher.output.toHex();

              key_arr.push({
                'email': csv_arr[1],
                'doc_id': sha256,
                'doc_key': forge.util.bytesToHex(aes_iv) 
                         + forge.util.bytesToHex(aes_key),
              });

              cc_arr.push({
                'user_id': csv_arr[0],
                'sha256': sha256,
                'aes256': aes256
              });

              bar.animate(parseFloat(i+1) / parseFloat(lines.length), {duration: 800}, function(){});
            });

            bar.set(1);
            viewModel.csvTotalCertifications(cc_arr.length);
            viewModel.csvToSendCertChain(JSON.stringify(cc_arr))
            //viewModel.csvToDownloadAdmin(JSON.stringify(key_arr));
            viewModel.csvInProgress(false);
          };
        })(evt.target.files[0]);
        reader.readAsText(evt.target.files[0]);
      } else {
        alert('You can only work with one CSV manifest at a time.');
      }
    }

    document.getElementById('csv_file_upload').addEventListener('change', handleFileSelect, false);
  } else {
    // TODO: Make user aware of this, disable inputs.
    alert('The File APIs are not fully supported in this browser.');
  }

  // Initialize CSV file progress bar (only once per page load).
  var bar = new ProgressBar.Circle(csv_progress_bar, {
    color: '#000',
    // This has to be the same size as the maximum width to
    // prevent clipping
    strokeWidth: 4,
    trailWidth: 1,
    easing: 'easeInOut',
    duration: 1400,
    text: {
      autoStyleContainer: false
    },
    from: { color: '#aaa', width: 1 },
    to: { color: '#333', width: 4 },
    // Set default step function for all animate calls
    step: function(state, circle) {
      circle.path.setAttribute('stroke', state.color);
      circle.path.setAttribute('stroke-width', state.width);

      var value = Math.round(circle.value() * 100);
      if (value === 0) {
        circle.setText('');
      } else {
        circle.setText(value + '%');
      }

    }
  });
  bar.text.style.fontFamily = 'Helvetica, sans-serif';
  bar.text.style.fontSize = '2rem';

  // Bind the Knockout view model.
  ko.applyBindings(viewModel);
});