from django.conf.urls import include, url
from django.contrib import admin
from internal import views

urlpatterns = [
  url(r'^chain/$', views.chain, name='chain'),
  url(r'^network/$', views.network, name='network'),
  url(r'^add_node/$', views.add_node, name='add_node'),
  url(r'^remove_node/$', views.remove_node, name='remove_node'),
  url(r'^approve_peer_request/$', views.approve_peer_request, name='approve_peer_request'),
  url(r'^request_peer/$', views.request_peer, name='request_peer'),
  url(r'^end_peering/$', views.end_peering, name='end_peering'),
  url(r'^certify/$', views.certify, name='certify'),
  url(r'^manage_certifications/$', views.manage_certifications, name='manage_certifications'),
  url(r'^queue_certifications/$', views.queue_certifications, name='queue_certifications'),
  url(r'^revoke_document/$', views.revoke_document, name='revoke_document'),
  url(r'^discard_pending_block/$', views.discard_pending_block, name='discard_pending_block'),
  url(r'^submit_pending_block/$', views.submit_pending_block, name='submit_pending_block'),
  url(r'^user_options/$', views.user_options, name='user_options'),
]