from __future__ import division
import requests, json, base64
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.http import Http404
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.conf import settings
from internal.shared import create_rpc_url
from django import forms
import os, hashlib, csv

#@login_required
def chain(request):
  try:
    pending_blocks_resp = requests.get(create_rpc_url('/pending_blocks'))
    hashchain_resp = requests.get(create_rpc_url('/hashchain'))
    return render(request, 'internal/chain.html', {
      'pending_blocks' : pending_blocks_resp.json(),
      'processing_block': hashchain_resp.json()[0],
      'latest_n_hcblocks': hashchain_resp.json()[1]
    })
  except Exception as ex:
    messages.error(request, 'Your institution\'s CertChain node \
      is not available at this time.')
    return render(request, 'internal/chain.html', {})

#@login_required
def network(request):
  try:
    resp = requests.get(create_rpc_url('/network'))
    return render(request, 'internal/network.html',\
      {'network' : resp.json()})
  except Exception as ex:
    messages.error(request, 'Your institution\'s CertChain node \
      is not available at this time.')
    return render(request, 'internal/network.html', {})

#@login_required
def add_node(request):
  if request.method == 'POST':
    payload = {
      'hostname': request.POST['hostname'],
      'port': request.POST['port'],
      'address': request.POST['address']
    }
    resp = requests.post(create_rpc_url('/add_node'),
      data=json.dumps(payload))
    if resp.status_code == 200 and resp.text == 'OK':
      messages.success(request,\
        'The node was added to the list of known nodes.')
    else:
      messages.error(request,\
        'An error occurred while adding the node you specified: '
        + str(resp.text))
    return redirect(reverse('internal:overview'))
  raise Http404

#@login_required
def remove_node(request):
  if request.method == 'POST':
    inst_addr = request.POST['inst_addr']
    resp = requests.post(create_rpc_url('/remove_node/' + inst_addr))
    if resp.status_code == 200 and resp.text == 'OK':
      messages.success(request,\
        'The node was removed from the list of known nodes.')
    else:
      messages.error(request,\
        'An error occurred while removing the node you specified: '
        + str(resp.text))
    return redirect(reverse('internal:overview'))
  raise Http404

#@login_required
def approve_peer_request(request):
  if request.method == 'POST':
    addr = request.POST['requesting_addr']
    resp = requests.post(create_rpc_url('/approve_peerreq/' + addr))
    if resp.status_code == 200 and resp.text == 'OK':
      messages.success(request,\
        'Your approval was submitted successfully.')
    else:
      messages.error(request,\
        'An error occurred while processing your \
        peer request approval: ' + resp.text)
    return redirect(reverse('internal:overview'))
  raise Http404

#@login_required
def request_peer(request):
  if request.method == 'POST':
    addr = request.POST['addr']
    resp = requests.post(create_rpc_url('/request_peer/' + addr))
    if resp.status_code == 200 and resp.text == 'OK':
      messages.success(request,\
        'Your peering request was successfully submitted.')
    else:
      messages.error(request,\
        'An error occurred while processing your \
        peer request: ' + resp.text)
    return redirect(reverse('internal:overview'))
  raise Http404

#@login_required
def end_peering(request):
  if request.method == 'POST':
    addr = request.POST['addr']
    resp = requests.post(create_rpc_url('/end_peering/' + addr))
    if resp.status_code == 200 and resp.text == 'OK':
      messages.success(request,\
        'Your peering termination request was successfully submitted.')
    else:
      messages.error(request,\
        'An error occurred while processing your \
        peering termination request: ' + resp.text)
    return redirect(reverse('internal:overview'))
  raise Http404

#@login_required
def certify(request):
  return render(request, 'internal/certify.html', {})

#@login_required
def manage_certifications(request):
  try:
    resp = requests.get(create_rpc_url('/all_certifications'))
    return render(request, 'internal/certifications.html',\
      {'certifications' : resp.json()})
  except Exception as ex:
    messages.error(request, 'Your institution\'s CertChain node \
      is not available at this time.')
    return render(request, 'internal/certifications.html', {})

#@login_required
def queue_certifications(request):
  if request.method == 'POST':
    payload = request.POST['certify_payload']
    resp = requests.post(create_rpc_url('/certify'),
      data=payload)
    if resp.status_code == 200 and resp.text == 'OK':
      messages.success(request,\
        'The certifications have been submitted successfully as a new pending block.')
      return redirect(reverse('internal:chain'))
    else:
      messages.error(request,\
        'An error occurred while processing your \
        certification request: ' + resp.text)
      return redirect(reverse('internal:certify'))
  raise Http404

#@login_required
def revoke_document(request):
  if request.method == 'POST':
    docid = request.POST['docid_to_revoke']
    resp = requests.post(create_rpc_url('/revoke/' + docid))
    if resp.status_code == 200:
      messages.success(request,\
        'Your document revocation request has been submitted as a new pending block.')
      return redirect(reverse('internal:chain'))
    else:
      messages.error(request,\
        'An error occurred while processing your revocation \
        request: ' + str(resp.status_code))
    return redirect(reverse('internal:manage_certifications'))
  raise Http404

#@login_required
def discard_pending_block(request):
  if request.method == 'POST':
    discard_id = request.POST['discard_id']
    resp = requests.post(create_rpc_url('/discard_pending_block/' + discard_id))
    if resp.status_code == 200:
      messages.success(request,\
        'The pending block was successfully discarded.')
    else:
      messages.error(request,\
        'An error occurred while processing your discard \
        request: ' + str(resp.status_code))
    return redirect(reverse('internal:chain'))
  raise Http404

#@login_required
def submit_pending_block(request):
  if request.method == 'POST':
    submit_id = request.POST['submit_id']
    resp = requests.post(create_rpc_url('/submit_pending_block/' + submit_id))
    if resp.status_code == 200:
      messages.success(request,\
        'The pending block was successfully submitted.')
    else:
      messages.error(request,\
        'An error occurred while processing your submission \
        request: ' + str(resp.status_code))
    return redirect(reverse('internal:chain'))
  raise Http404

#@login_required
def user_options(request):
  return render(request, 'internal/user_options.html', {})