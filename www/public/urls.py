from django.conf.urls import include, url
from public import views

urlpatterns = [
  url(r'^sso/$', views.sso, name='sso'),
  url(r'^sso_login/$', views.sso_login, name='sso_login'),
  url(r'^transcript/(?P<docid>.+?)/$', views.transcript, name='transcript'),
  url(r'^diploma/(?P<docid>.+?)/$', views.diploma, name='diploma'),
  url(r'^raw_document/(?P<docid>.+?)/$', views.raw_document, name='raw_document'),
  url(r'^raw_block_by_hash/(?P<block_hash>.+?)/$', views.raw_block_by_hash, name='raw_block_by_hash'),
  url(r'^raw_block_at_height/(?P<block_height>.+?)/$', views.raw_block_at_height, name='raw_block_at_height'),
  url(r'^student/(?P<student_id>.+?)/$', views.student, name='student'),
]