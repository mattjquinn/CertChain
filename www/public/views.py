import requests, json, base64
from django.shortcuts import render, redirect
from django.contrib import messages
from django.core.urlresolvers import reverse
from internal.shared import create_rpc_url
from collections import defaultdict
from django.http import HttpResponse, Http404
import datetime

import logging
logger = logging.getLogger('public')

def index(request):
  return render(request, 'index.html', {})

def sso(request):
  return render(request, 'public/sso.html', {})

# This is a stand-in for SSO to allow us to
# switch users during demonstrations. Password
# is deliberately ignored.
def sso_login(request):
  if request.method == 'POST':
    username = request.POST['username']
    if username == 'mquinn':
      request.session['USERNAME'] = 'mquinn'
      request.session['FULL_NAME'] = 'Matt Quinn'
      return redirect(reverse('internal:chain'))
    elif username == 'tblack':
      request.session['USERNAME'] = 'tblack'
      request.session['FULL_NAME'] = 'Tom Black'
      return redirect(reverse('internal:chain'))
    else:
      messages.error(request, 'The username you entered is invalid.')
    return redirect(reverse('public:sso'))
  raise Http404

def get_document(request, docid, type):
  try:
      resp = requests.get(create_rpc_url('/document/' + docid))
      json = resp.json()
      return render(request, 'public/'+ type + '.html',
        {'docid' : docid, 'doc' : json['contents'], 'raw_data': resp.text})
  except Exception as ex:
    messages.error(request, 'Unable to retrieve ' + type + ' at this time: ' + str(ex))
    return redirect(reverse('internal:manage_certifications'))

def diploma(request, docid):
  return get_document(request, docid, 'diploma')

def transcript(request, docid):
  return get_document(request, docid, 'transcript')

# Allows public users to see raw JSON data via link;
# we cannot link directly to the RPC port as that is
# not intended to be publicly accessible.
def raw_document(request, docid):
  resp = requests.get(create_rpc_url('/document/' + docid))
  return HttpResponse(resp.text, content_type='application/json')

def raw_block_at_height(request, block_height):
  resp = requests.get(create_rpc_url('/block_at_height/' + block_height))
  return HttpResponse(resp.text, content_type='application/json')

def raw_block_by_hash(request, block_hash):
  resp = requests.get(create_rpc_url('/block_by_hash/' + block_hash))
  return HttpResponse(resp.text, content_type='application/json')

def student(request, student_id):
  try:
    resp = requests.get(create_rpc_url('/certifications_by_student_id/' + student_id))
    certs_by_type = defaultdict(list)
    for c in resp.json():
      certs_by_type[c['doc_type']].append(c)
    return render(request, 'public/student.html',\
      {'certs_by_type' : certs_by_type.iteritems(),
       'student_id': student_id,
      })
  except Exception as ex:
    messages.error(request, 'Your institution\'s CertChain node \
      is not available at this time: ' + str(ex))
    return render(request, 'public/student.html', {'student_id': student_id})