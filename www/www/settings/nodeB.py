from .base import *

ALLOWED_HOSTS = ['stanford', 'localhost']
INSTITUTION_CERTCHAIN_NODE_HOSTNAME = 'localhost'
INSTITUTION_CERTCHAIN_NODE_RPC_PORT = '5001'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates'),
                 os.path.join(BASE_DIR, 'templates/stanford')],
        'APP_DIRS': True,
        # TODO: Make this false in production!
        'OPTIONS': {
          'debug': True,
          'context_processors': [
            'django.template.context_processors.request',
            'django.contrib.auth.context_processors.auth',
            'django.contrib.messages.context_processors.messages',
          ],
        },
    },
]

# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db_stanford.sqlite3'),
    }
}