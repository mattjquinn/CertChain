from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.auth import views as auth_views
from public.views import index as index_view

urlpatterns = [
    #url(r'^admin/', include(admin.site.urls)),
    #url(r'^$', auth_views.login),
    #url(r'^logout/', auth_views.logout),
    url(r'^$', index_view),
    url(r'^internal/', include('internal.urls', namespace='internal')),
    url(r'^public/', include('public.urls', namespace='public')),
]